package com.godelivery.customer.fragments.dialog;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker;
import com.godelivery.R;
import com.godelivery.databinding.DialogFragmentScheduleBinding;

import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class ScheduleDialogFragment extends DialogFragment {
    private DialogFragmentScheduleBinding binding;
    private Date dateSelected;
    private   CharSequence FORMAT_24_HOUR = "EEE d MMM H:mm a";
    private String stringDate = "";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DialogFragmentScheduleBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
    }

    private void setListeners() {
        binding.tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!stringDate.isEmpty()) {
                    getTargetFragment().onActivityResult(300, RESULT_OK, new Intent().
                            putExtra("stringDate", stringDate).
                            putExtra("date", dateSelected));
                    dismiss();
                } else {
                    Toast.makeText(getActivity(), R.string.select_date, Toast.LENGTH_SHORT).show();
                }
            }
        });

        binding.dateTimePicker.mustBeOnFuture();
        binding.dateTimePicker.setStepMinutes(1);

        Date targetTime = new Date(); //now
        if(getArguments().getBoolean("pickDrop")) {
            targetTime.setMinutes(targetTime.getMinutes() + 10);
        }
//        binding.dateTimePicker.dat
        binding.dateTimePicker.setMinDate(targetTime);

        binding.dateTimePicker.setListener(new SingleDateAndTimePicker.Listener() {
            @Override
            public void onDateChanged(String displayed, Date date) {
                dateSelected = date;
                stringDate = displayed;
            }
        });

//        binding.dateTimePicker.setTimeListener(targetTime);

        dateSelected = binding.dateTimePicker.getDate();
        stringDate = DateFormat.format(FORMAT_24_HOUR, binding.dateTimePicker.getDate()).toString();
    }

}
