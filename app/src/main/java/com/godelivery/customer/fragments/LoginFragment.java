package com.godelivery.customer.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.activities.HomeActivity;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.databinding.FragmentLoginBinding;

import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment implements View.OnClickListener {
    private FragmentLoginBinding binding;
    private EditText etEmailForgotPWD;
    private AlertDialog alertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentLoginBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListeners();
        setSpannable();
    }

    private void setSpannable() {
        String source = binding.tvSignUp.getText().toString();
        Spannable spannable = new SpannableString(source);
        spannable.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)), source.indexOf("Sign up"),
                source.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        binding.tvSignUp.setText(spannable);
    }

    private void setListeners() {
        binding.btnLogin.setOnClickListener(this);
        binding.tvForgotPassword.setOnClickListener(this);
        binding.tvSignUp.setOnClickListener(this);
        binding.ivPasswordToggle.setOnClickListener(this);
        binding.etPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                String email = binding.etEmail.getText().toString().trim();
                String password = binding.etPassword.getText().toString().trim();
                if (GeneralFunctions.validateEmail(getActivity(), email, binding.etEmail) &&
                        GeneralFunctions.validatePassword(getActivity(), password, binding.etPassword)) {
                    apiLogin(email, password);
                }
              /*  startActivity(new Intent(getActivity(),HomeActivity.class));
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.flip_in,R.anim.flip_out);*/
                break;
            case R.id.tvForgotPassword:
                openDialogForgotPassword();
                break;
            case R.id.tvSignUp:
                GeneralFunctions.addFragmentWithAnimation(getActivity(), android.R.id.content, new SignUpFragment(), true, true, Constants.SLIDE_ALL);
                break;
            case R.id.tvSubmit:
                String emailForgotPassword = etEmailForgotPWD.getText().toString().trim();
                if (GeneralFunctions.validateEmail(getActivity(), emailForgotPassword, etEmailForgotPWD)) {
                    apiForgotPassword(emailForgotPassword);
                }
                break;
            case R.id.ivPasswordToggle:
                if (binding.etPassword.getTransformationMethod() instanceof PasswordTransformationMethod) {
                    binding.etPassword.setTransformationMethod(null);
                } else {
                    binding.etPassword.setTransformationMethod(new PasswordTransformationMethod());
                }
                binding.etPassword.setSelection(binding.etPassword.getText().length());
                break;
        }
    }

    private void apiLogin(String email, String password) {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
            GeneralFunctions.showDialog(getActivity());
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("email", email);
            hashMap.put("password", password);
            hashMap.put("domain_name", "Envios.godelivery.com.do");
            RestClient.get().login(hashMap).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            if (response.body().data != null && response.body().data.vendor_details != null) {
                                saveData(response.body().data);
                                startActivity(new Intent(getActivity(), HomeActivity.class));
                                getActivity().finish();
                                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            } else {
                                Toast.makeText(getActivity(), getString(R.string.incorrect_email_pwd), Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            try {
                                Toast.makeText(getActivity(),
                                        new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        t.printStackTrace();
                        Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    private void openDialogForgotPassword() {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        final View dialogView = View.inflate(getActivity(), R.layout.dialog_forgot_password, null);
        etEmailForgotPWD = dialogView.findViewById(R.id.etEmailForgotPWD);
        TextView tvSubmit = dialogView.findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(this);
        alertBuilder.setView(dialogView);
        alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    private void apiForgotPassword(String emailForgotPassword) {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
            GeneralFunctions.showDialog(getActivity());
            RestClient.get().forgotPassword(emailForgotPassword).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            Toast.makeText(getActivity(), R.string.forgot_password_msg, Toast.LENGTH_SHORT).show();
                            alertDialog.dismiss();
                        } else {
                            try {
                                Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                        t.printStackTrace();
                    }
                }
            });
        }
    }


    private void saveData(Data data) {
        AppController.mPrefs.save(Constants.PROFILE_DATA, data);
        AppController.mPrefs.save(Constants.ACCESS_TOKEN, data.access_token);
        Log.e("token", data.access_token);
    }


}
