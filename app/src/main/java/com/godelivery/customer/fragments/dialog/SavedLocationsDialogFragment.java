package com.godelivery.customer.fragments.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.adapters.SavedLocationsAdapter;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.Prefs;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.FavLocationData;
import com.godelivery.databinding.DialogFragmentSavedLocationsBinding;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class SavedLocationsDialogFragment extends DialogFragment {

    private DialogFragmentSavedLocationsBinding binding;
    private String from = "";
    private List<FavLocationData> favLocationDataList = new ArrayList<>();

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DialogFragmentSavedLocationsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar();
        setListeners();
        if (getArguments() != null)
            from = getArguments().getString("from");
        setAdapter(favLocationDataList);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                apiGetAddress(null, "");
            }
        }, 200);
    }


    private void apiGetAddress(final LatLng latLng, final String address) {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), false)) {
            GeneralFunctions.showDialog(getActivity());
            RestClient.get().getLocations(AppController.mPrefs.getString(Constants.ACCESS_TOKEN, "")).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            if (response.body().data != null && response.body().data.favLocations != null && response.body().data.favLocations.size() > 0) {
                                favLocationDataList.clear();
                                favLocationDataList.addAll(response.body().data.favLocations);
                                Collections.reverse(favLocationDataList);
                                binding.rvSavedLocations.getAdapter().notifyDataSetChanged();

                            } else {
                                if (latLng != null) {
                                    favLocationDataList.clear();
                                    FavLocationData favLocationData = new FavLocationData();
                                    favLocationData.address = address;
                                    favLocationData.latitude = String.valueOf(latLng.latitude);
                                    favLocationData.longitude = String.valueOf(latLng.longitude);
                                    favLocationDataList.add(favLocationData);
                                    List<FavLocationData> list = new Gson().fromJson(Prefs.with(getActivity()).
                                            getString("locationList", ""), new TypeToken<List<FavLocationData>>() {
                                    }.getType());
                                    if (list != null && list.size() > 0) {
                                        favLocationDataList.addAll(list);
                                    }
                                    Collections.reverse(favLocationDataList);

                                    binding.rvSavedLocations.getAdapter().notifyDataSetChanged();
                                    Prefs.with(getActivity()).save("locationList", new Gson().toJson(favLocationDataList));
                                }
                            }
                        } else {
                            try {
                                Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        t.printStackTrace();
                        Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void setListeners() {
        binding.tvSelectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLocationSelector();
            }
        });
    }

    private void openLocationSelector() {
        /*try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(getActivity());
            startActivityForResult(intent, Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }*/

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(getActivity()), Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void setAdapter(List<FavLocationData> fav_locations) {
        binding.rvSavedLocations.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvSavedLocations.setAdapter(new SavedLocationsAdapter(getActivity(), fav_locations, from));
        binding.rvSavedLocations.setClipToPadding(false);
        binding.rvSavedLocations.setPadding(0, 0, 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.activity_vertical_margin));
        binding.rvSavedLocations.setFocusableInTouchMode(false);
        binding.rvSavedLocations.setNestedScrollingEnabled(false);
    }

    private void setToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        if (getTargetFragment() != null) {
            switch (getTargetRequestCode()) {
                case 120:
                    binding.tvTitle.setText(R.string.select_pick_up_loc);
                    break;
                case 130:
                    binding.tvTitle.setText(R.string.select_drop_off_loc);
                    break;
                default:
                    binding.tvTitle.setText(R.string.saved_loc);
                    break;
            }
        }
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(getActivity(), data);
                //Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                LatLng latLng = place.getLatLng();
                String address = place.getName() + "-" + place.getAddress().toString();
                apiAddAddress(latLng, address);
                if (!from.isEmpty()) {
                    getTargetFragment().
                            onActivityResult(getTargetRequestCode(), RESULT_OK,
                                    new Intent().
                                            putExtra("lat",
                                                    Double.valueOf(place.getLatLng().latitude)).
                                            putExtra("lng",
                                                    Double.valueOf(place.getLatLng().longitude))
                                            .putExtra("address",
                                                    address));
                    dismissAllowingStateLoss();
                }

            }
        }
    }

    private void apiAddAddress(final LatLng latLng, final String address) {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), false)) {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("access_token", AppController.mPrefs.getString(Constants.ACCESS_TOKEN, ""));
            hashMap.put("latitude", String.valueOf(latLng.latitude));
            hashMap.put("longitude", String.valueOf(latLng.longitude));
            hashMap.put("address", address);
            RestClient.get().addLocation(hashMap).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        if (from.isEmpty()) {
                            GeneralFunctions.dismissDialog();
                            if (response.isSuccessful()) {
                                apiGetAddress(latLng, address);
                            } else {
                                try {
                                    Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        if (from.isEmpty()) {
                            GeneralFunctions.dismissDialog();
                            t.printStackTrace();
                            Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });
        }
    }
}
