package com.godelivery.customer.fragments.dialog;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.godelivery.R;
import com.godelivery.customer.activities.BookingActivity;
import com.godelivery.customer.adapters.MaxWeightAdapter;
import com.godelivery.databinding.DialogFragmentHistoryBinding;

public class MaxWeightDialogFragment extends DialogFragment {
    private DialogFragmentHistoryBinding binding;
    private Dialog dialog;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dialog = new Dialog(getActivity(), R.style.CustomDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DialogFragmentHistoryBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar();
        setAdapter();
    }

    private void setAdapter() {
        /*List<Data> datas = new Gson().fromJson(getArguments().getString("data"), new TypeToken<List<Data>>() {
        }.getType());*/
        binding.rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.textHint.setVisibility(View.GONE);
        binding.rvHistory.setAdapter(new MaxWeightAdapter(getActivity(), BookingActivity.vehicleList,dialog));
        binding.rvHistory.setClipToPadding(false);
        binding.rvHistory.setPadding(0, 0, 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.activity_vertical_margin));
    }

    private void setToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.tvTitle.setText(R.string.max_volume);

        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                BookingActivity.mAdapter.notifyDataSetChanged();
            }
        });

    }
}
