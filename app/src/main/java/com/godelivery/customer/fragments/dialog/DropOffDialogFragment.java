package com.godelivery.customer.fragments.dialog;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.godelivery.BuildConfig;
import com.godelivery.R;
import com.godelivery.customer.adapters.NameAdapter;
import com.godelivery.customer.adapters.ReferenceImageAdapter;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.PassNameData;
import com.godelivery.customer.utils.RetrofitUtils;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Datum;
import com.godelivery.customer.webservices.pojo.NameData;
import com.godelivery.customer.webservices.pojo.PickupDropOffModel;
import com.godelivery.databinding.DialogFragmentDropOffBinding;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.godelivery.customer.utils.Constants.IMAGE_DIRECTORY_NAME;

public class DropOffDialogFragment extends DialogFragment implements View.OnClickListener, PassNameData {
    private final String[] mPermission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private DialogFragmentDropOffBinding binding;
    public LatLng latLng = null;
    private String address = "", dateString = "";
    private List<String> filePaths = new ArrayList<>();
    private List<String> fileUrls = new ArrayList<>();
    private PickupDropOffModel pickupDropOffModel = new PickupDropOffModel();
    private String countryCode = "+1";
    private LatLng latLngSource, latLngDriver;
    private  ArrayList<Datum> nameData = new ArrayList<>();
    private boolean showName = true;
    private Handler handler = new Handler();
    public static PassNameData passNameData;

    public static Bitmap getResizedBitmapLessThan500KB(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();


        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        Bitmap reduced_bitmap = Bitmap.createScaledBitmap(image, width, height, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        reduced_bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] imageInByte = stream.toByteArray();
        long lengthbmp = imageInByte.length / 1024;
        if (lengthbmp > (1024)) {
            return getResizedBitmapLessThan500KB(reduced_bitmap, (int) lengthbmp);
        } else {
            return reduced_bitmap;
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        passNameData = this;
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DialogFragmentDropOffBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar();
        setListeners();
        setAdapter();
        setViews();

        binding.etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(binding.etName.getText().toString().trim().length()>0) {
                    apiVendor(binding.etName.getText().toString());
                }else{
                    binding.rvName.setVisibility(View.GONE);
                }
            }
        });

        binding.etName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result) {
                    case EditorInfo.IME_ACTION_SEARCH:
                        try {
                            if(binding.etName.getText().toString().trim().length()>0) {
                                apiVendor(binding.etName.getText().toString());
                            }else{
                                binding.rvName.setVisibility(View.GONE);
                            }
                        }catch (Exception exc){
                            exc.printStackTrace();
                        }
                        break;
                    default:
                }
                return true;
            }
        });
    }

    private void setViews() {
        if (getArguments() != null && getArguments().containsKey("dropoffData")) {
            PickupDropOffModel pickupDropOffModel = new Gson().fromJson(getArguments().getString("dropoffData"), PickupDropOffModel.class);
            binding.etName.setText(pickupDropOffModel.name);
            if (pickupDropOffModel.email != null && !pickupDropOffModel.email.isEmpty())
                binding.etEmail.setText(pickupDropOffModel.email);
            binding.etPhoneNumber.setText(pickupDropOffModel.phoneNumber);
            binding.tvCountryCode.setText(pickupDropOffModel.countryCode);
            countryCode = pickupDropOffModel.countryCode;
            binding.etPickUpTime.setText(pickupDropOffModel.pickUpTime);
            binding.etPickUpAddress.setText(getArguments().getString("address"));
            binding.etHouseAddress.setText(pickupDropOffModel.address);
            latLng = new LatLng(getArguments().getDouble("lat"), getArguments().getDouble("lng"));
            fileUrls.addAll(pickupDropOffModel.referenceImagePath);
            binding.rvImages.getAdapter().notifyDataSetChanged();
        } else {
            latLng = new LatLng(getArguments().getDouble("lat"), getArguments().getDouble("lng"));
            binding.etPickUpAddress.setText(getArguments().getString("address"));

//            if(PickUpPointDialogFragment.latLng!=null && latLng !=null){
//                getEstTimeAndDistance(PickUpPointDialogFragment.latLng, latLng);
//            }
        }
    }

    private void setAdapter() {
        binding.rvImages.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        binding.rvImages.setAdapter(new ReferenceImageAdapter(getActivity(), fileUrls));
    }

    private void setListeners() {
        binding.btnDone.setOnClickListener(this);
        binding.etPickUpTime.setOnClickListener(this);
        binding.ivChooseImage.setOnClickListener(this);
        binding.etPickUpAddress.setOnClickListener(this);
        binding.tvCountryCode.setOnClickListener(this);

    }

    private void setToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvCountryCode:
                openPicker();
                break;
            case R.id.ivChooseImage:
                checkForPermissions();
                break;
            case R.id.etPickUpTime:
                ScheduleDialogFragment scheduleDialogFragment = new ScheduleDialogFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putBoolean("pickDrop", false);
                scheduleDialogFragment.setArguments(bundle2);
                scheduleDialogFragment.setTargetFragment(this, 300);
                scheduleDialogFragment.show(getFragmentManager(), ScheduleDialogFragment.class.getName());
                break;
            case R.id.etPickUpAddress:
                SavedLocationsDialogFragment savedLocationsDialogFragment = new SavedLocationsDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("from", PickUpPointDialogFragment.class.getName());
                savedLocationsDialogFragment.setArguments(bundle);
                savedLocationsDialogFragment.setTargetFragment(this, 200);
                savedLocationsDialogFragment.show(getFragmentManager(), SavedLocationsDialogFragment.class.getName());
                break;
            case R.id.btnDone:

                String name = binding.etName.getText().toString().trim();
                String email = binding.etEmail.getText().toString().trim();
                String phoneNumber = binding.etPhoneNumber.getText().toString().trim();
                String pickUpTime = binding.etPickUpTime.getText().toString().trim();

                try {
                    SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                    SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
                    Date date1 = parseFormat.parse(pickUpTime);
                    pickUpTime  =  displayFormat.format(date1);
                }catch (Exception exc){
                    exc.printStackTrace();
                }

                String pickUpAddress = binding.etPickUpAddress.getText().toString().trim();
                String houseAddress = binding.etHouseAddress.getText().toString().trim();
                SimpleDateFormat date =new SimpleDateFormat("yyyy-MM-dd hh:mm");//2017-10-30 10:00);
                Date pickup= null;

                try {
                    pickup = date.parse(PickUpPointDialogFragment.time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
               // PickupDropOffModel pickupDropOffModel = new PickupDropOffModel();
                if (GeneralFunctions.validateName(getActivity(), name, binding.etName)) {
                    if (!email.isEmpty()) {
                        if (GeneralFunctions.validateEmail(getActivity(), email, binding.etEmail) &&
                                GeneralFunctions.validateMobileNumber(getActivity(), phoneNumber, binding.etPhoneNumber) &&
                                GeneralFunctions.checkForEmpty(getActivity(), pickUpTime, getString(R.string.select_time)) &&
                                GeneralFunctions.checkForEmpty(getActivity(), pickUpAddress, getString(R.string.enter_pick_up_address))) {

                            Date drop=null;
                            try {
                                drop = date.parse(pickUpTime);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(drop.before(pickup)) {
                                Toast.makeText(getActivity(), R.string.select_valid_date, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                pickupDropOffModel.name = name;
                                if (!houseAddress.isEmpty())
                                    pickupDropOffModel.address = houseAddress;
//                                try {
//                                    SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//                                    SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
//                                    Date date1 = parseFormat.parse(pickUpTime);
//                                    pickUpTime  =  displayFormat.format(date1);
//                                }catch (Exception exc){
//                                    exc.printStackTrace();
//                                }
                                pickupDropOffModel.pickUpTime = pickUpTime;
                                pickupDropOffModel.location = pickUpAddress;
                                pickupDropOffModel.email = email;
                                pickupDropOffModel.phoneNumber = phoneNumber;
                                pickupDropOffModel.countryCode = countryCode;
                                pickupDropOffModel.lat = String.valueOf(latLng.latitude);
                                pickupDropOffModel.lng = String.valueOf(latLng.longitude);

                                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, new Intent().
                                        putExtra("dropOffData",
                                                new Gson().toJson(pickupDropOffModel)));
                                dismiss();
                            }
                        }
                    } else {
                        if (GeneralFunctions.validateMobileNumber(getActivity(), phoneNumber, binding.etPhoneNumber) &&
                                GeneralFunctions.checkForEmpty(getActivity(), pickUpTime, getString(R.string.select_times)) &&
                                GeneralFunctions.checkForEmpty(getActivity(), pickUpAddress, getString(R.string.pick_up_address))) {
                            Date drop=null;
                            try {
                                drop = date.parse(pickUpTime);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(drop.before(pickup)) {
                                Toast.makeText(getActivity(), R.string.valid_date, Toast.LENGTH_SHORT).show();
                            }
                            else {
                                pickupDropOffModel.name = name;
                                if (!houseAddress.isEmpty())
                                    pickupDropOffModel.address = houseAddress;

//                                try {
//                                    SimpleDateFormat displayFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//                                    SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
//                                    Date date1 = parseFormat.parse(pickUpTime);
//                                    pickUpTime  =  displayFormat.format(date1);
//                                }catch (Exception exc){
//                                    exc.printStackTrace();
//                                }

                                pickupDropOffModel.pickUpTime = pickUpTime;
                                pickupDropOffModel.location = pickUpAddress;
                                pickupDropOffModel.phoneNumber = phoneNumber;
                                pickupDropOffModel.countryCode = countryCode;
                                pickupDropOffModel.lat = String.valueOf(latLng.latitude);
                                pickupDropOffModel.lng = String.valueOf(latLng.longitude);
                                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, new Intent().
                                        putExtra("dropOffData",
                                                new Gson().toJson(pickupDropOffModel)));
                                dismiss();
                            }
                        }
                    }
                }
                break;
        }
    }

    private void openPicker() {
        final CountryPicker picker = CountryPicker.newInstance(getString(R.string.select_country));  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                countryCode = dialCode;
                binding.tvCountryCode.setText(countryCode);
                picker.dismiss();
            }
        });
        picker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            latLng = new LatLng(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
            address = data.getStringExtra("address");
            binding.etPickUpAddress.setText(address);
        } else if (requestCode == 300) {
            Date date = (Date) data.getSerializableExtra("date");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a", Locale.getDefault());
            dateString = dateFormat.format(date);
            binding.etPickUpTime.setText(dateString);
        } else if (requestCode == Constants.GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
            if (data != null) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePaths.add(cursor.getString(columnIndex));
                    cursor.close();
                    File file = new File(filePaths.get(filePaths.size() - 1));
                    long length = file.length() / 1024; // Size in KB
                    if (length > 1024) {
                        Bitmap image = null;
                        image = BitmapFactory.decodeFile(file.getAbsolutePath());
                        Bitmap bits = getResizedBitmapLessThan500KB(image, 1024);
                        file = saveImageToExternalStorage(bits);
                        apiUploadreferenceImage(file.getAbsolutePath());
                    } else
                        apiUploadreferenceImage(filePaths.get(filePaths.size() - 1));
                }
            }
        } else if (requestCode == Constants.CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            File file = new File(filePaths.get(filePaths.size() - 1));
            long length = file.length() / 1024; // Size in KB
            if (length > 1024) {
                Bitmap image = null;
                image = BitmapFactory.decodeFile(file.getAbsolutePath());
                Bitmap bits = getResizedBitmapLessThan500KB(image, 700);
                file = saveImageToExternalStorage(bits);
                apiUploadreferenceImage(file.getAbsolutePath());
            } else
                apiUploadreferenceImage(filePaths.get(filePaths.size() - 1));

        }
    }

    private void apiUploadreferenceImage(String s) {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), false)) {
            GeneralFunctions.showDialog(getActivity());
            HashMap<String, RequestBody> hashMap = new HashMap<>();
            hashMap.put("ref_image\";filename=\"img.jpg", RetrofitUtils.imageToRequestBody(new File(s)));
            RestClient.get().uploadImage(hashMap).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            fileUrls.add(response.body().data.ref_image);
                            pickupDropOffModel.referenceImagePath = fileUrls;
                            binding.rvImages.getAdapter().notifyDataSetChanged();
                        } else {
                            try {
                                Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        t.printStackTrace();
                        Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    }

    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), mPermission[0]) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(mPermission, Constants.REQUEST_CODE_PERMISSION);
        } else {
            selectImage();
        }
    }

    private File saveImageToExternalStorage(Bitmap finalBitmap) {
        File file2;
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        file2 = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        try {
            FileOutputStream out;
            out = new FileOutputStream(file2);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            return file2;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file2;
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_CODE_PERMISSION) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        showDialogOK(getString(R.string.permissions_required_storage),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts(Constants.SETTING_URI_SCHEME,
                                                        getActivity().getPackageName(), null);
                                                intent.setData(uri);
                                                startActivityForResult(intent, Constants.REQUEST_CODE_SETTINGS);
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                }, Constants.NEVER_ASK);//take to settings
                        // user also CHECKED "never ask again"
                    } else if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permission)) {
                        showDialogOK(getString(R.string.permissions_required_storage),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                checkForPermissions();
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                }, Constants.DENY);//ask permission again
                        // user did NOT check "never ask again"
                    }
                } else {
                    selectImage();
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener, String from) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity())
                .setMessage(message);

        if (from.equals(Constants.DENY)) {
            alertDialog.setTitle(R.string.title_permission_dialog)
                    .setPositiveButton(R.string.button_ok_permission_dialog, okListener)
                    .setNegativeButton(getString(R.string.cancel), okListener)
                    .create()
                    .show();
        } else if (from.equals(Constants.NEVER_ASK)) {
            alertDialog.setTitle(R.string.title_permission_dialog)
                    .setPositiveButton(R.string.button_setting_permission_dialog, okListener)
                    .setNegativeButton(getString(R.string.cancel), okListener)
                    .create()
                    .show();
        }

    }

    private File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");
        return mediaFile;
    }

    private File saveImageToExternalStorage(Bitmap finalBitmap, File file, boolean isGallery) {
        File file2;
        if (!isGallery) {
            file2 = file;
        } else {
            File mediaStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            file2 = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        }
        try {
            FileOutputStream out;
            out = new FileOutputStream(file2);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
            out.flush();
            out.close();
            return file2;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file2;
    }


    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_gallary),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(items[0])) {
                    Intent takePictureIntent = new Intent(
                            MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        File f = getOutputMediaFile();
                        if (f != null) {
                            filePaths.add(f.getAbsolutePath());
                            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                                    BuildConfig.APPLICATION_ID + ".provider", f);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    startActivityForResult(takePictureIntent, Constants.CAMERA_REQUEST_CODE);
                } else if (items[item].equals(items[1])) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, Constants.GALLERY_REQUEST_CODE);

                } else if (items[item].equals(items[2])) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Bitmap getFile(String imgPath) {
        Bitmap bMapRotate = null;
        try {

            if (imgPath != null) {
                ExifInterface exif = new ExifInterface(imgPath);

                int mOrientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 1);

                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgPath, options);
                options.inSampleSize = calculateInSampleSize(options, 400, 400);
                options.inJustDecodeBounds = false;

                bMapRotate = BitmapFactory.decodeFile(imgPath, options);
                if (mOrientation == 6) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                } else if (mOrientation == 8) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(270);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                } else if (mOrientation == 3) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(180);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                }
            }

        } catch (OutOfMemoryError e) {
            bMapRotate = null;
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            bMapRotate = null;
            e.printStackTrace();
        }
        return bMapRotate;
    }

    private int calculateInSampleSize(BitmapFactory.Options options,
                                      int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }


//    private void getEstTimeAndDistance(LatLng latLng, LatLng latLngdes) {
//
//        String origin = 30.7333 + "," + 76.7794;
//        String destination = 28.7041 + "," + 77.1025;
//
////        String origin = latLng.latitude + "," + latLng.longitude;
////        String destination = latLngdes.latitude + "," + latLngdes.longitude;
//
//        RestClient.get().getPath(origin, destination, "AIzaSyBbQGK4XowBRcKgkvTXHmkGZBghx5HfsO4")
//                .enqueue(new Callback<MapDirectionsPojo>() {
//                    @Override
//                    public void onResponse(Call<MapDirectionsPojo> call, Response<MapDirectionsPojo> response) {
//                        if (response.isSuccessful() && response.body().status.equals(getString(R.string.oks))) {
////                             response.body().routes.get(0).duration.text;
//
////                                     try {
////                                         Date targetTime = new Date(); //now
////                                         targetTime.setMinutes(targetTime.getMinutes() + 10);
////                                         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm a",
////                                                 Locale.getDefault());
////                                         dateString = dateFormat.format(targetTime);
////                                         binding.etPickUpTime.setText(dateString);
////                                     }catch (Exception exc){
////                                         exc.printStackTrace();
////                                     }
//
//
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<MapDirectionsPojo> call, Throwable t) {
//                        t.printStackTrace();
//                    }
//                });
//    }


    private void apiVendor(String name) {
        if(showName) {
            if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
//            GeneralFunctions.showDialog(getActivity());
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("access_token", "" + AppController.mPrefs.getString(Constants.ACCESS_TOKEN, ""));
                hashMap.put("customer_name", name);

                RestClient.get().vendorSearch(hashMap).enqueue(new Callback<NameData>() {
                    @Override
                    public void onResponse(Call<NameData> call, Response<NameData> response) {
                        if (getActivity() != null) {
//                        GeneralFunctions.dismissDialog();
                            if (response.isSuccessful()) {
                                nameData.clear();
                                nameData.addAll(response.body().data);
                                binding.rvName.setVisibility(View.VISIBLE);
//                            binding.rvName.getAdapter().notifyDataSetChanged();
                                binding.rvName.setLayoutManager(new LinearLayoutManager(getActivity()));
                                binding.rvName.setAdapter(new NameAdapter(getActivity(), nameData, false));

                            } else {
                                binding.rvName.setVisibility(View.GONE);
                                try {
                                    Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<NameData> call, Throwable t) {
                        binding.rvName.setVisibility(View.GONE);
                        if (getActivity() != null) {
//                        GeneralFunctions.dismissDialog();
                            t.printStackTrace();
                            Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
    }

    @Override
    public void doSomething(Datum datum) {
        showName = false;


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showName = true;
            }
        }, 400);
        GeneralFunctions.hideKeyboard(getActivity());
        binding.rvName.setVisibility(View.GONE);
        pickupDropOffModel.name = datum.customer_username;
        pickupDropOffModel.email = datum.customer_email;

        String[] separated = datum.customer_phone.split(" ");
        pickupDropOffModel.countryCode = separated[0];
        pickupDropOffModel.phoneNumber = separated[1];

        binding.etPickUpAddress.setText(datum.job_address);
        latLng = new LatLng(Double.parseDouble(datum.job_latitude), Double.parseDouble(datum.job_longitude));

        binding.etName.setText(pickupDropOffModel.name);
        if (pickupDropOffModel.email != null && !pickupDropOffModel.email.isEmpty())
            binding.etEmail.setText(pickupDropOffModel.email);
        binding.etPhoneNumber.setText(pickupDropOffModel.phoneNumber);
        binding.tvCountryCode.setText(pickupDropOffModel.countryCode);
        countryCode = pickupDropOffModel.countryCode;
    }

}
