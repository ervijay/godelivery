package com.godelivery.customer.fragments.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.Prefs;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.databinding.LayoutDriverDetailsBinding;
import com.google.gson.Gson;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverDetailsDialogFragment extends DialogFragment implements View.OnClickListener {
    private LayoutDriverDetailsBinding binding;
    private String number = " ";
    private Data driverData;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = LayoutDriverDetailsBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        driverData = new Gson().fromJson(getArguments().getString("driverData"), Data.class);
        number = driverData.fleet_phone;
        setListeners();
        setViews();
    }

    private void setViews() {
        binding.tvDriverName.setText(driverData.fleet_name);
        binding.tvBaseFare.setText("$" + Prefs.with(getActivity()).getString("price", ""));
        binding.tvSourceDate.setText(Prefs.with(getActivity()).getString("pickUpTime", ""));
        binding.tvSourceName.setText(Prefs.with(getActivity()).getString("pickUpLocation", ""));
        binding.tvDestName.setText(Prefs.with(getActivity()).getString("dropOffLocation", ""));
        binding.tvDestDate.setText(Prefs.with(getActivity()).getString("dropOffTime", ""));
        binding.tvVehicleType.setText(Prefs.with(getActivity()).getString("vehicle", ""));
        binding.tvVehicle.setText(Prefs.with(getActivity()).getString("vehicle", ""));
        GeneralFunctions.setRoundImage(getActivity(), driverData.fleet_thumb_image, binding.ivProfilePic, R.drawable.ic_profile);

    }

    private void setListeners() {
        binding.tvCall.setOnClickListener(this);
        binding.tvMessage.setOnClickListener(this);
        binding.tvCancel.setOnClickListener(this);
        binding.activityDriver.setOnClickListener(this);
        binding.back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvMessage:
                //Intent sendIntent = new Intent(Intent.ACTION_VIEW);
                //sendIntent.setData(Uri.parse("sms:"+number));
                break;
            case R.id.back:
                getActivity().finish();
                break;
            case R.id.tvCall:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null));
                startActivity(intent);
                break;
            case R.id.tvCancel:
                cancelTask();
                break;
            case R.id.activityDriver:
               /* RateDriverDialogFragment rateDriverDialogFragment = new RateDriverDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("driverData", getArguments().getString("driverData"));
                rateDriverDialogFragment.setArguments(bundle);
                rateDriverDialogFragment.show(getActivity().getSupportFragmentManager(), RateDriverDialogFragment.class.getName());
               */ break;
        }
    }


    private void cancelTask() {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
            GeneralFunctions.showDialog(getActivity());
            RestClient.get().cancelTask(new HashMap<String, String>()).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            Prefs.with(getActivity()).remove("driverData");
                            getActivity().finish();
                        } else {
                            Toast.makeText(getActivity(), R.string.failed, Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        t.printStackTrace();
                        Toast.makeText(getActivity(), getString(R.string.api_failure_error), Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
    }
}
