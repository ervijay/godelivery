package com.godelivery.customer.fragments.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.adapters.HistoryAdapter;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonListPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.databinding.DialogFragmentHistoryBinding;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HistoryDialogFragment extends DialogFragment {
    private DialogFragmentHistoryBinding binding;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        return dialog;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DialogFragmentHistoryBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar();
        apiGetHistory();

    }

    private void apiGetHistory() {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), false)) {
            GeneralFunctions.showDialog(getActivity());
            Data data = AppController.mPrefs.getObject(Constants.PROFILE_DATA, Data.class);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("access_token", AppController.mPrefs.getString(Constants.ACCESS_TOKEN, ""));
            //hashMap.put("vendor_id",String.valueOf(data.vendor_details.vendor_id));
            hashMap.put("vendor_id", AppController.mPrefs.getObject(Constants.PROFILE_DATA,Data.class).vendor_details.vendor_id+"");
            hashMap.put("layout_type", String.valueOf(0));
            RestClient.get().history(hashMap).enqueue(new Callback<CommonListPojo>() {
                @Override
                public void onResponse(Call<CommonListPojo> call, Response<CommonListPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            if(response.body().aaData!=null&&response.body().aaData.size()>0) {
                                setAdapter(response.body().aaData);
                                binding.textHint.setVisibility(View.GONE);
                            }  else
                                binding.textHint.setVisibility(View.VISIBLE);
                        } else {
                            try {
                                binding.textHint.setVisibility(View.VISIBLE);
                                Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonListPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        t.printStackTrace();
                        Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void setAdapter(List<List<String>> aaData) {
        binding.rvHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.rvHistory.setAdapter(new HistoryAdapter(getActivity(), aaData));
        binding.rvHistory.setClipToPadding(false);
        binding.rvHistory.setPadding(0, 0, 0, getActivity().getResources().getDimensionPixelOffset(R.dimen.activity_vertical_margin));
    }

    private void setToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

}
