package com.godelivery.customer.fragments;


import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.activities.BookingActivity;
import com.godelivery.customer.fragments.dialog.DropOffDialogFragment;
import com.godelivery.customer.fragments.dialog.PickUpPointDialogFragment;
import com.godelivery.customer.fragments.dialog.SavedLocationsDialogFragment;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.Prefs;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonListPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.customer.webservices.pojo.PickupDropOffModel;
import com.godelivery.databinding.FragmentHomeBinding;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnMapReadyCallback,
        View.OnClickListener {
    private final String[] mPermission = {Manifest.permission.ACCESS_FINE_LOCATION};
    private FragmentHomeBinding binding;
    private MapView mapView;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private AlertDialog.Builder alertDialog;
    private AlertDialog alertDialogConfirm;
    private GoogleMap mGoogleMap;
    private boolean isAllPermissionsGiven = false;
    private PickupDropOffModel pickUpData;
    private PickupDropOffModel dropOfData;
    private int i = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // ((HomeActivity) getActivity()).setInterface(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        initializeMap(savedInstanceState);
        return binding.getRoot();
    }

    private void initializeMap(@Nullable Bundle savedInstanceState) {
        try{
        binding.mapView.onCreate(savedInstanceState);
        binding.mapView.onResume();
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        initMap();}
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    private void initMap() {
        binding.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                mGoogleMap.getUiSettings().setCompassEnabled(false);
                mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
                addMarkersToMap(mGoogleMap);
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView = binding.mapView;
        setListeners();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        apiNearbyDrivers();
    }



    private void apiNearbyDrivers() {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
            //   GeneralFunctions.showDialog(getActivity());
            RestClient.get().nearbyDriver(AppController.mPrefs.getString(Constants.LATITUDE, ""),
                    AppController.mPrefs.getString(Constants.LONGITUDE, "0")).enqueue(new Callback<CommonListPojo>() {
                @Override
                public void onResponse(Call<CommonListPojo> call, Response<CommonListPojo> response) {
                    GeneralFunctions.dismissDialog();
                    if (response.isSuccessful()) {
                        addSourceAndDestMarker(response.body().data);
                    }
                }

                @Override
                public void onFailure(Call<CommonListPojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.api_failure_error), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    private void addMarkersToMap(GoogleMap mGoogleMap) {
        mGoogleMap.clear();
        try {
            LatLng latLng = new LatLng(Double.parseDouble(AppController.mPrefs.getString(Constants.LATITUDE,
                    "")),
                    Double.parseDouble(AppController.mPrefs.getString(Constants.LONGITUDE, "")));
            mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_your_location)));
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11f));
        }catch (Exception exc){
            exc.printStackTrace();
            try {
                Data data = AppController.mPrefs.getObject(Constants.PROFILE_DATA, Data.class);
                LatLng latLng = new LatLng(Double.parseDouble(data.vendor_details.latitude),
                        Double.parseDouble(data.vendor_details.longitude));
                mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_your_location)));
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11f));
            }catch (Exception exc1){
                exc1.printStackTrace();
            }
        }
    }

    private void addSourceAndDestMarker(List<Data> data) {
        mGoogleMap.clear();
        LatLng latLng = new LatLng(Double.parseDouble(AppController.mPrefs.getString(Constants.LATITUDE, "0")), Double.parseDouble(AppController.mPrefs.getString(Constants.LONGITUDE, "0")));
        mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_your_location)));
        LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();
        latLngBounds.include(latLng);
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                LatLng latLngDest = new LatLng(Double.valueOf(data.get(i).latitude),
                        Double.valueOf(data.get(i).longitude));
                Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(latLngDest)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_top)));
                marker.setRotation(i * 10);
                latLngBounds.include(latLngDest);
            }
        }
        int width = getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (width * 0.20);
        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds.build(), width, mapView.getHeight(), padding));
    }

    private void setListeners() {
        binding.tvDropOffLocation.setOnClickListener(this);
        binding.tvPickupLocation.setOnClickListener(this);
        binding.ivCurrentLocation.setOnClickListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        if (Prefs.with(getContext()).getBoolean("bConfirm", false)) {
            pickUpData = null;
            dropOfData = null;
            binding.tvDropOffLocation.setText("");
            binding.tvPickupLocation.setText("");
            Prefs.with(getContext()).remove("bConfirm");
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         if (requestCode == 600) {
            pickUpData = new Gson().fromJson(data.getStringExtra("pickUpData"), PickupDropOffModel.class);
            binding.tvPickupLocation.setText(pickUpData.location);
            if (dropOfData != null) {
                startActivity(new Intent(getActivity(), BookingActivity.class)
                        .putExtra("pickUpData", new Gson().toJson(pickUpData))
                        .putExtra("dropOffData", new Gson().toJson(dropOfData)));

            } else {
                SavedLocationsDialogFragment savedLocationsDialogFragment1 = new SavedLocationsDialogFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("from", DropOffDialogFragment.class.getName());
                savedLocationsDialogFragment1.setArguments(bundle1);
                savedLocationsDialogFragment1.setTargetFragment(this, 130);
                savedLocationsDialogFragment1.show(getFragmentManager(), SavedLocationsDialogFragment.class.getName());
            }

        } else if (requestCode == 500) {
            dropOfData = new Gson().fromJson(data.getStringExtra("dropOffData"), PickupDropOffModel.class);
            binding.tvDropOffLocation.setText(dropOfData.location);

            if (pickUpData != null) {
                startActivityForResult(new Intent(getActivity(), BookingActivity.class)
                        .putExtra("pickUpData", new Gson().toJson(pickUpData))
                        .putExtra("dropOffData", new Gson().toJson(dropOfData)), Constants.BOOKING_DONE);
            }
        } else if (requestCode == Constants.BOOKING_DONE) {

        } else if (requestCode == 120 && resultCode == RESULT_OK) {
            LatLng latLngSaved = new LatLng(Double.parseDouble(AppController.mPrefs.getString(Constants.LATITUDE, "0")), Double.parseDouble(AppController.mPrefs.getString(Constants.LONGITUDE, "0")));
            LatLng latLng = new LatLng(data.getDoubleExtra("lat", 0), data.getDoubleExtra("lng", 0));
            float distance=distanceBtwPoint(latLngSaved,latLng);
            if(distance>100){
                openDialogChangeLocation(data);
            } else {
                openPickUp(data);
            }
        } else if (requestCode == 130 && resultCode == RESULT_OK) {
            DropOffDialogFragment dropOffDialogFragment = new DropOffDialogFragment();
            dropOffDialogFragment.setTargetFragment(this, 500);
            Bundle bundle = new Bundle();
            if (dropOfData != null) {
                bundle.putString("dropoffData", new Gson().toJson(dropOfData));
            }
            bundle.putDouble("lat", data.getDoubleExtra("lat", 0));
            bundle.putDouble("lng", data.getDoubleExtra("lng", 0));
            bundle.putString("address", data.getStringExtra("address"));
            dropOffDialogFragment.setArguments(bundle);
            dropOffDialogFragment.show(getFragmentManager(), DropOffDialogFragment.class.getName());
        }

    }
    private float distanceBtwPoint(LatLng current, LatLng destination) {


        Location loc1 = new Location("");
        loc1.setLatitude(current.latitude);
        loc1.setLongitude(current.longitude);

        Location loc2 = new Location("");
        loc2.setLatitude(destination.latitude);
        loc2.setLongitude(destination.longitude);
        float distanceInMeters = loc1.distanceTo(loc2);

        return distanceInMeters;


    }
    private void openPickUp(Intent data) {
        PickUpPointDialogFragment pickUpPointDialogFragment = new PickUpPointDialogFragment();
        pickUpPointDialogFragment.setTargetFragment(this, 600);
        Bundle bundle = new Bundle();
        if (pickUpData != null) {
            bundle.putString("pickupData", new Gson().toJson(pickUpData));
        }
        bundle.putDouble("lat", data.getDoubleExtra("lat", 0));
        bundle.putDouble("lng", data.getDoubleExtra("lng", 0));
        bundle.putString("address", data.getStringExtra("address"));
        pickUpPointDialogFragment.setArguments(bundle);
        pickUpPointDialogFragment.show(getFragmentManager(), PickUpPointDialogFragment.class.getName());
    }

    private void openDialogChangeLocation(final Intent data) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
        View alertView = View.inflate(getActivity(), R.layout.layout_pick_up_change, null);
        TextView tvConfirm = alertView.findViewById(R.id.btnConfirm);
        TextView tvCancel = alertView.findViewById(R.id.btnCancel);
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openPickUp(data);
                alertDialogConfirm.dismiss();
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogConfirm.dismiss();
            }
        });
        alertBuilder.setView(alertView);
        alertBuilder.setCancelable(false);
        alertDialogConfirm = alertBuilder.create();
        alertDialogConfirm.show();
    }


    private void showDialogOK(String message, DialogInterface.OnClickListener okListener, String from) {
        alertDialog = new AlertDialog.Builder(getActivity())
                .setMessage(message).setCancelable(false);
        switch (from) {
            case Constants.DENY:
                showDialog(R.string.title_permission_dialog, R.string.button_ok_permission_dialog, okListener);
                break;
            case Constants.NEVER_ASK:
                showDialog(R.string.title_permission_dialog, R.string.button_setting_permission_dialog, okListener);
                break;
            default:
                showDialog(R.string.title_gps_on_dialog, R.string.button_ok_gps_on_dialog, okListener);
                break;
        }

    }

    private void showDialog(int title_permission_dialog, int button_ok_permission_dialog,
                            DialogInterface.OnClickListener okListener) {
        alertDialog.setTitle(title_permission_dialog)
                .setPositiveButton(button_ok_permission_dialog, okListener)
                .create()
                .show();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvPickupLocation:
                SavedLocationsDialogFragment savedLocationsDialogFragment = new SavedLocationsDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("from", PickUpPointDialogFragment.class.getName());
                savedLocationsDialogFragment.setArguments(bundle);
                savedLocationsDialogFragment.setTargetFragment(this, 120);
                savedLocationsDialogFragment.show(getFragmentManager(), SavedLocationsDialogFragment.class.getName());

                break;
            case R.id.tvDropOffLocation:
                SavedLocationsDialogFragment savedLocationsDialogFragment1 = new SavedLocationsDialogFragment();
                Bundle bundle1 = new Bundle();
                bundle1.putString("from", DropOffDialogFragment.class.getName());
                savedLocationsDialogFragment1.setArguments(bundle1);
                savedLocationsDialogFragment1.setTargetFragment(this, 130);
                savedLocationsDialogFragment1.show(getFragmentManager(), SavedLocationsDialogFragment.class.getName());
                break;
            case R.id.ivCurrentLocation:
                addMarkersToMap(mGoogleMap);
                break;
        }
    }

}
