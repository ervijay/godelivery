package com.godelivery.customer.fragments.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.Prefs;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.databinding.LayoutRatingDriverBinding;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RateDriverDialogFragment extends DialogFragment {
    private LayoutRatingDriverBinding binding;
    private Data driverData;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity(), R.style.CustomDialogAnimation);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setWindowAnimations(R.style.CustomDialogAnimation);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = LayoutRatingDriverBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        driverData = new Gson().fromJson(getArguments().getString("driverData"), Data.class);
        setViews();
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismissAllowingStateLoss();
            }
        });

        binding.tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                apiRateDriver();
            }
        });
        String path = "http://maps.googleapis.com/maps/api/staticmap?size=600x600&path=40.737102,-73.990318|40.749825,-73.987963|40.752946,-73.987384|40.755823,-73.986397&sensor=false";
        GeneralFunctions.setImage(getActivity(), path, binding.ivStaticMap, R.color.colorPrimaryDark);

    }

    private void apiRateDriver() {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
            GeneralFunctions.showDialog(getActivity());
            RestClient.get().rateDriver(driverData.job_hash,
                    String.valueOf(binding.rbDriverRating.getRating()), "Nice").enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    GeneralFunctions.dismissDialog();
                    if (response.isSuccessful()) {
                        getActivity().finish();
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(getActivity(), getString(R.string.api_failure_error), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    private void setViews() {
        binding.tvDriverName.setText(driverData.fleet_name);
        binding.tvPrice.setText("$" + Prefs.with(getActivity()).getString("price", ""));
        binding.tvVehicleType.setText(Prefs.with(getActivity()).getString("vehicle", ""));
        GeneralFunctions.setRoundImage(getActivity(), driverData.fleet_thumb_image, binding.ivProfilePic, R.drawable.ic_profile);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Prefs.with(getActivity()).remove("driverData");
    }
}
