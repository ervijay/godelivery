package com.godelivery.customer.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.godelivery.BuildConfig;
import com.godelivery.R;
import com.godelivery.customer.activities.HomeActivity;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.RetrofitUtils;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.databinding.FragmentSignUpBinding;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.godelivery.customer.utils.Constants.IMAGE_DIRECTORY_NAME;


public class SignUpFragment extends Fragment implements View.OnClickListener {
    private final String[] mPermission = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    private FragmentSignUpBinding binding;
    private File fileProfilePic;
    private String picturePath = "";
    private String countryCode = "+1";
    private PlacePicker.IntentBuilder builder;
    public double lat;
    public double lng;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentSignUpBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setToolbar();
        setListeners();
        binding.tvCompany.setVisibility(View.VISIBLE);
        binding.etCompany.setVisibility(View.VISIBLE);
        binding.tvAddress.setVisibility(View.VISIBLE);
        binding.etAddress.setVisibility(View.VISIBLE);
    }

    private void setToolbar() {
        binding.toolbar.setNavigationIcon(R.drawable.ic_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    private void setListeners() {
        binding.btnLogin.setOnClickListener(this);
        binding.ivPicImage.setOnClickListener(this);
        binding.tvCountryCode.setOnClickListener(this);
        binding.etAddress.setOnClickListener(this);
//        binding.etAddress.setEnabled(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                String name = binding.etName.getText().toString().trim();
                String lname = binding.etLName.getText().toString().trim();
                String phoneNumber = binding.etPhoneNumber.getText().toString().trim();
                String email = binding.etEmail.getText().toString().trim();
                String company = "";
                company  = binding.etCompany.getText().toString().trim();
                String address = binding.etAddress.getText().toString().trim();



                String password = binding.etPassword.getText().toString().trim();
                if (GeneralFunctions.validateName(getActivity(), name, binding.etName) &&
                        GeneralFunctions.validateName(getActivity(), lname, binding.etLName) &&
                GeneralFunctions.validateMobileNumber(getActivity(), phoneNumber, binding.etPhoneNumber)
                        && GeneralFunctions.validateEmail(getActivity(), email, binding.etEmail) &&
//                        GeneralFunctions.validateCompany(getActivity(), company, binding.etCompany) &&
                        GeneralFunctions.validateAddress(getActivity(), address, binding.etAddress) &&
                        GeneralFunctions.validatePassword(getActivity(), password, binding.etPassword)) {
                    if (fileProfilePic != null) {
                        apiUploadImage(name, lname, countryCode + phoneNumber, email, company, address, password);
                    } else {
                        apiSignUp(name, lname,countryCode + phoneNumber, email, company, address, password, "");
                    }
                }
                break;
            case R.id.ivPicImage:
                checkForPermissions();
                break;
            case R.id.tvCountryCode:
                openPicker();
                break;

            case R.id.etAddress:
                try {
                    builder = new PlacePicker.IntentBuilder();
                    startActivityForResult(builder.build(getActivity()), Constants.PLACE_PICKER_REQUEST);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void openPicker() {
        final CountryPicker picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                countryCode = dialCode;
                binding.tvCountryCode.setText(countryCode);
                picker.dismiss();
            }
        });
        picker.show(getActivity().getSupportFragmentManager(), "COUNTRY_PICKER");
    }


    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(getContext(), mPermission[0]) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(mPermission, Constants.REQUEST_CODE_PERMISSION);
        } else {
            selectImage();
        }
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constants.REQUEST_CODE_PERMISSION) {
            for (int i = 0, len = permissions.length; i < len; i++) {
                String permission = permissions[i];
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    // user rejected the permission
                    boolean showRationale = shouldShowRequestPermissionRationale(permission);
                    if (!showRationale) {
                        showDialogOK(getString(R.string.permissions_required_storage),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts(Constants.SETTING_URI_SCHEME,
                                                        getActivity().getPackageName(), null);
                                                intent.setData(uri);
                                                startActivityForResult(intent, Constants.REQUEST_CODE_SETTINGS);
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                }, Constants.NEVER_ASK);//take to settings
                        // user also CHECKED "never ask again"
                    } else if (Manifest.permission.WRITE_EXTERNAL_STORAGE.equals(permission)) {
                        showDialogOK(getString(R.string.permissions_required_storage),
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case DialogInterface.BUTTON_POSITIVE:
                                                checkForPermissions();
                                                break;
                                            case DialogInterface.BUTTON_NEGATIVE:
                                                dialog.dismiss();
                                                break;
                                        }
                                    }
                                }, Constants.DENY);//ask permission again
                        // user did NOT check "never ask again"
                    }
                } else {
                    selectImage();
                }
            }
        }
    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener, String from) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity())
                .setMessage(message);

        if (from.equals(Constants.DENY)) {
            alertDialog.setTitle(R.string.title_permission_dialog)
                    .setPositiveButton(R.string.button_ok_permission_dialog, okListener)
                    .setNegativeButton(getString(R.string.cancel), okListener)
                    .create()
                    .show();
        } else if (from.equals(Constants.NEVER_ASK)) {
            alertDialog.setTitle(R.string.title_permission_dialog)
                    .setPositiveButton(R.string.button_setting_permission_dialog, okListener)
                    .setNegativeButton(getString(R.string.cancel), okListener)
                    .create()
                    .show();
        }

    }

    private File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");
        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == Constants.GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {
                if (data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        picturePath = cursor.getString(columnIndex);
                        cursor.close();
                        fileProfilePic = saveImageToExternalStorage(getFile(picturePath), null, true);
                        if (fileProfilePic != null) {
                            GeneralFunctions.setRoundImage(getActivity(), fileProfilePic.getAbsolutePath(), binding.ivPicImage,
                                    R.drawable.ic_profile);
                        }
                    }
                }
            } else if (requestCode == Constants.CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
                if (picturePath != null) {
                    fileProfilePic = saveImageToExternalStorage(getFile(picturePath), new File(picturePath), false);
                    if (fileProfilePic != null) {
                        GeneralFunctions.setRoundImage(getActivity(), fileProfilePic.getAbsolutePath(),
                                binding.ivPicImage, R.drawable.ic_profile);
                    }
                }
            }else if (requestCode == Constants.PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);

                lat = place.getLatLng().latitude;
                lng = place.getLatLng().longitude;

                    binding.etAddress.setText(place.getName() + " " + place.getAddress());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File saveImageToExternalStorage(Bitmap finalBitmap, File file, boolean isGallery) {
        File file2;
        if (!isGallery) {
            file2 = file;
        } else {
            File mediaStorageDir = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null;
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
            file2 = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        }
        try {
            FileOutputStream out;
            out = new FileOutputStream(file2);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
            out.flush();
            out.close();
            return file2;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file2;
    }


    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_gallary),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(
                getActivity());
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(items[0])) {
                    Intent takePictureIntent = new Intent(
                            MediaStore.ACTION_IMAGE_CAPTURE);
                    try {
                        File f = getOutputMediaFile();
                        if (f != null) {
                            picturePath = f.getAbsolutePath();
                            Uri photoURI = FileProvider.getUriForFile(getActivity(),
                                    BuildConfig.APPLICATION_ID + ".provider", f);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        picturePath = null;
                    }
                    startActivityForResult(takePictureIntent, Constants.CAMERA_REQUEST_CODE);
                } else if (items[item].equals(items[1])) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, Constants.GALLERY_REQUEST_CODE);

                } else if (items[item].equals(items[2])) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private Bitmap getFile(String imgPath) {
        Bitmap bMapRotate = null;
        try {

            if (imgPath != null) {
                ExifInterface exif = new ExifInterface(imgPath);

                int mOrientation = exif.getAttributeInt(
                        ExifInterface.TAG_ORIENTATION, 1);

                final BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imgPath, options);
                options.inSampleSize = calculateInSampleSize(options, 400, 400);
                options.inJustDecodeBounds = false;

                bMapRotate = BitmapFactory.decodeFile(imgPath, options);
                if (mOrientation == 6) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(90);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                } else if (mOrientation == 8) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(270);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                } else if (mOrientation == 3) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(180);
                    bMapRotate = Bitmap.createBitmap(bMapRotate, 0, 0,
                            bMapRotate.getWidth(), bMapRotate.getHeight(),
                            matrix, true);
                }
            }

        } catch (OutOfMemoryError e) {
            bMapRotate = null;
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            bMapRotate = null;
            e.printStackTrace();
        }
        return bMapRotate;
    }

    private int calculateInSampleSize(BitmapFactory.Options options,
                                      int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private void apiSignUp(String name, String lname, String phoneNumber, String email, String company, String address, String password, String ref_image) {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {

            GeneralFunctions.showDialog(getActivity());
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("email", email);
            hashMap.put("password", password);
            hashMap.put("phone_no", phoneNumber);
            hashMap.put("first_name", name);
            hashMap.put("last_name", lname);
            hashMap.put("lat", ""+lat);
            hashMap.put("lng", ""+lng);
            hashMap.put("address", ""+address);
            hashMap.put("company", ""+company);
            hashMap.put("domain_name", "Envios.godelivery.com.do");
            if (!ref_image.isEmpty()) {
                hashMap.put("vendor_image", ref_image);
            }
            RestClient.get().signUp(hashMap).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            if (response.body().data != null && response.body().data.vendor_details != null) {
                                saveData(response.body().data);
                                startActivity(new Intent(getActivity(), HomeActivity.class));
                                getActivity().finish();
                                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                            }
                        } else {
                            try {
                                Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        t.printStackTrace();
                        Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }


    private void apiUploadImage(final String name, final String lname,final String phoneNumber, final String email, final String company,
                                final String address,
                                final String password) {
        if (GeneralFunctions.isConnectedToNetwork(getActivity(), true)) {
            GeneralFunctions.showDialog(getActivity());
            HashMap<String, RequestBody> hashMap = new HashMap<>();
            hashMap.put("ref_image\";filename=\"img.jpg", RetrofitUtils.imageToRequestBody(fileProfilePic));
            RestClient.get().uploadImage(hashMap).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        if (response.isSuccessful()) {
                            if (response.body().data != null && response.body().data.ref_image != null) {
                                apiSignUp(name, lname, phoneNumber, email, company, address, password, response.body().data.ref_image);
                            }
                        } else {
                            try {
                                Toast.makeText(getActivity(), new JSONObject(response.errorBody().string()).getString("message"),
                                        Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    if (getActivity() != null) {
                        GeneralFunctions.dismissDialog();
                        t.printStackTrace();
                        Toast.makeText(getActivity(), R.string.api_failure_error, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void saveData(Data data) {
        AppController.mPrefs.save(Constants.PROFILE_DATA, data);
        AppController.mPrefs.save(Constants.ACCESS_TOKEN, data.access_token);
    }
}
