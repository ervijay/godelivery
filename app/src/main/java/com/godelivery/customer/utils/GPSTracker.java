package com.godelivery.customer.utils;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import com.godelivery.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class GPSTracker extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private final String TAG = "SocialExchange";
    //GPS Tracker
    private Context mContext;
    private GoogleApiClient mGoogleApiClient;

    public GPSTracker(Context context) {
        this.mContext = context;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onDestroy() {
        // Disconnecting the client invalidates it.
        mGoogleApiClient.disconnect();
        super.onDestroy();
    }

    @Override
    public void onCreate() {
    }

    @Override
    public IBinder onBind(Intent arg0) {

        return null;
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000); // Update location every second
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, getString(R.string.google_connection_error));
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            Log.i("From service:", getString(R.string.location_recevied)+" " + location.toString());
            AppController.mPrefs.save(Constants.LATITUDE, location.getLatitude() + "");
            AppController.mPrefs.save(Constants.LONGITUDE, location.getLongitude() + "");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
