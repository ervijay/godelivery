package com.godelivery.customer.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.godelivery.R;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


public class GeneralFunctions {
    private static Dialog dialog;

    public static void addFragmentWithAnimation(Activity activity, int containerId,
                                                android.support.v4.app.Fragment fragment,
                                                boolean addToBackStack, boolean replace,
                                                int animNumber) {
        FragmentTransaction fragmentTransaction =
                ((AppCompatActivity) activity).getSupportFragmentManager().beginTransaction();

        switch (animNumber) {
            case Constants.BOUNCE:
                fragmentTransaction.setCustomAnimations(R.anim.bounce, 0, 0, 0);
                break;
            case Constants.SLIDE_ALL:
                fragmentTransaction.setCustomAnimations(R.anim.slide_in_right,
                        R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right);
                break;
            case Constants.NONE:
                break;
        }

        if (addToBackStack) {
            if (replace)
                fragmentTransaction.replace(containerId, fragment).addToBackStack("").commit();
            else
                fragmentTransaction.add(containerId, fragment).addToBackStack("").commit();

        } else {
            if (replace)
                fragmentTransaction.replace(containerId, fragment).commit();
            else
                fragmentTransaction.add(containerId, fragment).commit();
        }
    }


    public static boolean validateEmail(Activity activity, String email, EditText etEmail) {
        if (email.isEmpty()) {
            etEmail.setError(activity.getString(R.string.empty_email));
            etEmail.requestFocus();
            return false;
        } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.setError(activity.getString(R.string.invalid_email));
            etEmail.requestFocus();
            return false;
        } else {
            etEmail.setError(null);
            return true;
        }
    }

    public static boolean validatePassword(Activity activity, String password, EditText etPassword) {
        if (password.isEmpty()) {
            etPassword.setError(activity.getString(R.string.empty_password));
            etPassword.requestFocus();
            return false;
        } else if (password.length() < Constants.PASSWORD_MIN_LEN ||
                password.length() > Constants.PASSWORD_MAX_LEN) {
            etPassword.setError(activity.getString(R.string.invalid_password));
            etPassword.requestFocus();
            return false;
        } else {
            etPassword.setError(null);
            return true;
        }
    }

    public static boolean validateName(Activity activity, String name, EditText etName) {
        if (name.isEmpty()) {
            etName.setError(activity.getString(R.string.empty_name));
            etName.requestFocus();
            return false;
        } else {
            etName.setError(null);
            return true;
        }
    }

    public static boolean validateCompany(Activity activity, String name, EditText etName) {
        if (name.isEmpty()) {
            etName.setError(activity.getString(R.string.empty_company_name));
            etName.requestFocus();
            return false;
        } else {
            etName.setError(null);
            return true;
        }
    }

    public static boolean validateAddress(Activity activity, String name, TextView etName) {
        if (name.isEmpty()) {
            etName.setError(activity.getString(R.string.empty_address));
            etName.requestFocus();
            return false;
        } else {
            etName.setError(null);
            return true;
        }
    }

    public static boolean validateMobileNumber(Activity activity, String mobileNumber,
                                               EditText etMobileNumber) {
        if (mobileNumber.isEmpty()) {
            etMobileNumber.setError(activity.getString(R.string.empty_number));
            etMobileNumber.requestFocus();
            return false;
        } else if (Long.valueOf(mobileNumber) == 0) {
            etMobileNumber.setError(activity.getString(R.string.invalid_number));
            etMobileNumber.requestFocus();
            return false;
        } else if (mobileNumber.length() < 9 || mobileNumber.length() > 16) {
            etMobileNumber.setError(activity.getString(R.string.short_number));
            etMobileNumber.requestFocus();
            return false;
        } else {
            etMobileNumber.setError(null);
            return true;
        }
    }

    public static boolean isConnectedToNetwork(Activity context, boolean isShowToast) {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) context.
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if (mNetworkInfo == null) {
                if (isShowToast) {
                    showInternetDialog(context);
                    //showToast(context,context.getString(R.string.network_not_connected));
                }
                return false;
            } else
                return true;
        } catch (NullPointerException e) {
            return false;
        }
    }

    private static void showInternetDialog(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.on_internet)
                .setMessage(R.string.internent_is_required)
                .setNegativeButton(R.string.wifi, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setNeutralButton(R.string.mobile_data, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings$DataUsageSummaryActivity"));
                            activity.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void showDialog(Context context) {
        try{ dialog = new Dialog(context, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_progress);
        ProgressBar pbHeaderProgress = (ProgressBar) dialog.findViewById(R.id.pbHeaderProgress);
        pbHeaderProgress.getIndeterminateDrawable().
                setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        dialog.show();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void dismissDialog() {
      try{
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();}
            catch(Exception e)
            {
                e.printStackTrace();
            }
    }


    public static void setImageWithRoundedCorners(final Activity context, String uri, final ImageView imageView, int id) {
        Glide.with(context).load(uri).bitmapTransform(new RoundedCornersTransformation(context, 8, 0,
                RoundedCornersTransformation.CornerType.ALL)).placeholder(id).crossFade().into(imageView);
    }

    public static void setImageWithRoundedCorner(final Activity context, String uri, final ImageView imageView, int id) {
        Glide.with(context).load(uri).bitmapTransform(new RoundedCornersTransformation(context, 4, 0,
                RoundedCornersTransformation.CornerType.ALL)).placeholder(id).crossFade().into(imageView);
    }

    public static void setRoundImage(final Activity context, String uri, final ImageView imageView, int drawable) {
        Glide.with(context).load(uri).asBitmap().centerCrop()
                .placeholder(drawable).transform(new CropCircleTransformation(context)).into(imageView);
    }

    public static void setImage(final Activity context, String uri, final ImageView imageView, int id) {
        Glide.with(context).load(uri).placeholder(id).into(imageView);
    }


    public static boolean checkForEmpty(Activity activity, String text, String errorMsg) {
        if (text.isEmpty()) {
            Toast.makeText(activity, errorMsg, Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

   /* public static void popBackStackSupportFragment(Activity activity){
        ((AppCompatActivity)activity).getSupportFragmentManager().popBackStack();
    }


    public static boolean validateName(Activity activity,String name, EditText etName) {
        if(name.isEmpty()){
            etName.setError(activity.getString(R.string.empty_name));
            return false;
        }
        else if(!name.matches(Constants.NAME_PATTERN)){
            etName.setError(activity.getString(R.string.invalid_name));
            return false;
        }
        else{
            etName.setError(null);
            return true;
        }
    }

    public static boolean validateMobileNumber(Activity activity,String mobileNumber,
                                               EditText etMobileNumber) {
        if (mobileNumber.isEmpty()) {
            etMobileNumber.setError(activity.getString(R.string.empty_number));
            return false;
        }
        else if (Long.valueOf(mobileNumber)==0){
            etMobileNumber.setError(activity.getString(R.string.invalid_number));
            return false;
        }
        else if(mobileNumber.length()<9 || mobileNumber.length()>16){
            etMobileNumber.setError(activity.getString(R.string.short_number));
            return false;
        }
        else {
            etMobileNumber.setError(null);
            return true;
        }
    }
    public static boolean checkForEmpty(Activity activity,String text,
                                               EditText etTxt,String errorMsg) {
        if (text.isEmpty()) {
            etTxt.setError(errorMsg);
            return false;
        }
        else {
            etTxt.setError(null);
            return true;
        }
    }

    public static boolean isConnectedToNetwork(Activity context,boolean isShowToast) {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager)context.
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();
            if(mNetworkInfo==null){
                if(isShowToast){
                    showInternetDialog(context);
                    //showToast(context,context.getString(R.string.network_not_connected));
                }
                return false;
            }
            else
                return true;
        }
        catch (NullPointerException e){
            return false;
        }
    }

    public static void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void setImageWithRoundedCorners(final Activity context, String uri, final ImageView imageView, int id) {
        Glide.with(context).load(uri).bitmapTransform(new RoundedCornersTransformation(context,8, 0,
                RoundedCornersTransformation.CornerType.ALL)).placeholder(id).crossFade().into(imageView);
    }
     public static void setImageWithRoundedCorner(final Activity context, String uri, final ImageView imageView, int id) {
        Glide.with(context).load(uri).bitmapTransform(new RoundedCornersTransformation(context,4, 0,
                RoundedCornersTransformation.CornerType.ALL)).placeholder(id).crossFade().into(imageView);
    }

    public static void setRoundImage(final Activity context, String uri, final ImageView imageView,int drawable) {
        Glide.with(context).load(uri).asBitmap().centerCrop()
                .placeholder(drawable).transform(new CropCircleTransformation(context)).into(imageView);
    }

    public static void setImage(final Activity context, String uri, final ImageView imageView,int id) {
        Glide.with(context).load(uri).placeholder(id).into(imageView);
    }


   public static boolean checkRetrofitSuccess(Activity activity, Response<CommonPojo> response){
        if(response.isSuccessful()){
            switch (response.body().success){
                case Constants.SUCCESS_OK:
                    return  true;
                case Constants.SUCCESS_ERROR:
                    GeneralFunctions.dismissDialog();
                    GeneralFunctions.showToast(activity, response.body().msg);
                    return  false;
                case Constants.SUCCESS_TOKEN_EXPIRE:
                    GeneralFunctions.dismissDialog();
                    //showTokenExpireDialog();
                    GeneralFunctions.showToast(activity,response.body().msg);
                    activity.finishAffinity();
                    activity.startActivity(new Intent(activity, LoginSignUpActivity.class));
                    return false;
                case Constants.SUCCESS_BLOCKED:
                    GeneralFunctions.dismissDialog();
                    if(activity instanceof LoginSignUpActivity){
                        GeneralFunctions.showToast(activity,response.body().msg);
                    }
                    else{
                        GeneralFunctions.showToast(activity,response.body().msg);
                        activity.finishAffinity();
                        activity.startActivity(new Intent(activity, LoginSignUpActivity.class));
                    }
                    return false;
                case Constants.SUCCESS_CONTRACTOR_CHANGE:
                    GeneralFunctions.showToast(activity,response.body().msg);
                    if(activity instanceof HomeDrawerActivity){
                        Intent intent=new Intent(Constants.UPDATE_MSG_FRAGMENT);
                        activity.sendBroadcast(intent);
                    }
                    GeneralFunctions.popBackStackSupportFragment(activity);
                    return false;
               default:
                    GeneralFunctions.dismissDialog();
                    GeneralFunctions.showToast(activity,activity.getString(R.string.something_went_wrong));
                    break;
            }
            return false;

        }
        else{
            GeneralFunctions.showToast(activity, activity.getString(R.string.something_went_wrong));
            GeneralFunctions.dismissDialog();
            return false;
        }

    }

    private static void showTokenExpireDialog() {

    }

    public static void showDialog(Context context) {
        dialog= new Dialog(context, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_progress);
        ProgressBar pbHeaderProgress=(ProgressBar)dialog.findViewById(R.id.pbHeaderProgress);
        pbHeaderProgress.getIndeterminateDrawable().
                setColorFilter(ContextCompat.getColor(context,R.color.colorPrimary), PorterDuff.Mode.MULTIPLY);
        dialog.show();
    }

    public static void dismissDialog() {
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
    }

    private static void showInternetDialog(final Activity activity)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Turn on internet")
                .setMessage("Internet connection is required to access the app")
                .setNegativeButton("WIFI", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        activity.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                })
                .setNeutralButton("Mobile data", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            Intent intent = new Intent();
                            intent.setComponent(new ComponentName("com.android.settings","com.android.settings.Settings$DataUsageSummaryActivity"));
                            activity.startActivity(intent);
                        }
                        catch (Exception e){
                             e.printStackTrace();
                        }
                    }
                })
                .setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dismissDialog();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.
                getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static int dpToPx(int dp,Activity activity) {
        Resources resources = activity.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int)px;
    }
*//*

  public static int dpToPx(int dp,Activity activity) {
        Resources resources = activity.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int)px;
    }

    public static void setImage(final Activity context, String uri, final ImageView imageView,int id) {
        Glide.with(context).load(uri).placeholder(id).into(imageView);
    }

    public static void setRoundImage(final Activity context, String uri, final ImageView imageView,int drawable) {
        Glide.with(context).load(uri).asBitmap().centerCrop()
                .placeholder(drawable).transform(new CropCircleTransformation(context)).into(imageView);
    }

    public static void showDialog(Context context) {
        dialog= new Dialog(context, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_progress);
        ProgressBar pbHeaderProgress=(ProgressBar)dialog.findViewById(R.id.pbHeaderProgress);
        pbHeaderProgress.getIndeterminateDrawable().
                setColorFilter(ContextCompat.getColor(context,R.color.blueWarm),PorterDuff.Mode.MULTIPLY);
        dialog.show();
    }

    public static void dismissDialog() {
        if(dialog!=null && dialog.isShowing())
            dialog.dismiss();
    }
    public static String convertMilliSecondsToFormattedDate(Long milliSeconds,
                                                            SimpleDateFormat simpleDateFormat){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTimeInMillis(milliSeconds);
        return simpleDateFormat.format(calendar.getTime()).trim();
    }
*/

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm=(InputMethodManager)activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
