package com.godelivery.customer.utils;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class RetrofitUtils {
    private static final String MIME_TYPE_TEXT = "text/plain";
    private static final String MIME_TYPE_IMAGE = "image/*";
    private static final String MIME_TYPE_VIDEO = "video/mp4";

    public static RequestBody stringToRequestBody(String string) {
        return RequestBody.create(MediaType.parse(MIME_TYPE_TEXT), string);
    }

    public static RequestBody imageToRequestBody(File file) {
        return RequestBody.create(MediaType.parse(MIME_TYPE_IMAGE), file);
    }

    public static RequestBody videoToRequestBody(File file) {
        return RequestBody.create(MediaType.parse(MIME_TYPE_VIDEO), file);
    }

    public static String createFileName(File file) {
        if (file != null) {
            return "ref_image\"; filename=\"" + file.getName();
        }
        return "ref_image\";filename=\"img.jpg";
    }
}
