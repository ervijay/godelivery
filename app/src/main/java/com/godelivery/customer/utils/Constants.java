package com.godelivery.customer.utils;


public class Constants {
    //splash time
    public static final int SPLASH_DURATION = 3000;
    public static final int TIME_INTERVAL = 2000; // # milliseconds, desired time passed between two back presses.

    //animation constants
    public static final int NONE = 0;
    public static final int SLIDE_ALL = 1;
    public static final int BOUNCE = 2;


    //password length,regx patterns
    public static final int PASSWORD_MIN_LEN = 5;
    public static final int PASSWORD_MAX_LEN = 14;
    public static final String NUM_PATTERN = "0000000000";
    public static final String NAME_PATTERN = "[a-zA-Z0-9][a-zA-Z0-9 ]*";
    public static final String COMMON_PATTERN = "[a-zA-Z0-9][a-zA-Z0-9 ]*";
    public static final String COMMON2_PATTERN = "[a-zA-Z0-9.,][a-zA-Z0-9., ]*";
    public static final String COMMON3_PATTERN = "[-a-zA-Z0-9.,][-a-zA-Z0-9., ]*";

    //pref keys
    public static final String USER_TYPE = "userType";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lng";
    public static final String PROFILE_DATA = "profile";
    public static final String ACCESS_TOKEN = "accessToken";
    public static final String REMEMBER_LOGIN = "remMe";
    public static final String CITY = "city";
    public static final String COUNTRY = "country";
    public static final String PROFILE_CONTRACTOR = "contractorProfile";
    public static final String BID_COUNT = "bidCount";

    //user types
    public static final String USER_TYPE_CONSUMER = "1";
    public static final String USER_TYPE_CONTRACTOR = "2";

    //permission codes
    public static final int REQUEST_CODE_PERMISSION = 2;
    public static final int REQUEST_CODE_SETTINGS = 700;
    public static final int REQUEST_CHECK_SETTINGS = 100;
    public static final int VIDEO_REQUEST_CODE = 3;
    public static final int CAMERA_REQUEST_CODE = 1;
    public static final int PLACE_PICKER_REQUEST = 1111;


    public static final int GALLERY_REQUEST_CODE = 2;
    public static final int GALLERY_REQUEST_CODE_MEDIA = 4;
    public static final int CAMERA_REQUEST_CODE_MEDIA = 5;
    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 55;
    public static final int REQUEST_CODE_TEMPLATE_CREATED = 40;
    public static final int PICK_UP_CODE = 200;
    public static final int BOOKING_DONE = 150;

    //open setting scheme
    public static final String SETTING_URI_SCHEME = "package";

    //dialog constants
    public static final String NEVER_ASK = "never ask";
    public static final String DENY = "deny ask";

    //image constants
    public static final String IMAGE_DIRECTORY_NAME = "/Zinbids";
    public static final String COMPRESSED_VIDEO_DIRECTORY_NAME = "/compVideos";
    public static final String IMAGE_NAME_PREFIX = "Image-";
    public static final String IMAGE_NAME_SUFFIX = ".jpg";
    public static final String TEMP_IMAGE_NAME_PREFIX = "photo_task";


    //intent constants
    public static final String PATH_VIDEO = "path";

    //bundle constants
    public static final String NAME = "fullname";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String REFERRAL_CODE = "shared_referalcode";
    public static final String PHONE = "phone";
    public static final String SUBCAT = "subCat";
    public static final String CAT = "cat";
    public static final String SERVICE_NAME = "sname";
    public static final String POSITION = "positionClicked";
    public static final String POST_DETAILS = "post_detail";
    public static final String IMAGE_PATH = "imagePath";
    public static final String FROM = "from";
    public static final String CHAT_USER_DETAILS = "chatUser";

    //retrofit constants
    public static final int SUCCESS_OK = 1;
    public static final int SUCCESS_ERROR = 0;
    public static final int SUCCESS_BLOCKED = 4;
    public static final int SUCCESS_TOKEN_EXPIRE = 3;
    public static final int SUCCESS_CONTRACTOR_CHANGE = 6;


    //intent filter keys
    public static final String UPDATE_PROFILE = "updateProfile";
    public static final String UPDATE_TASK_DETAILS = "updateTaskDetails";
    public static final String UPDATE_HOME_CONTRACTOR = "updateHomeContractor";
    public static final String UPDATE_MSG_FRAGMENT = "updateMsgFragment";
    public static final String UPDATE_PHOTO_VIDEOS_LIST = "updateMediaList";

}
