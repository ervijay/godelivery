package com.godelivery.customer.utils;

public class Config {

    private static String BASE_URL = "";
    private static AppMode appMode = AppMode.LIVE;

    static public String getBaseURL() {
        init(appMode);
        return BASE_URL;
    }

    private static void init(AppMode appMode) {
        //http://34.224.237.223:4000/api/v3/user/signup
        switch (appMode) {
            case DEV:
                BASE_URL = "http://34.224.237.223";
                break;

            case TEST:
                BASE_URL = "http://34.224.237.223";
                break;

            case LIVE:
                BASE_URL = "http://34.224.237.223";
                break;
        }
    }

    private enum AppMode {
        DEV, TEST, LIVE
    }

}