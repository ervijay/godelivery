package com.godelivery.customer.utils;

import android.app.Application;

public class AppController extends Application {
    public static Prefs mPrefs;

    @Override
    public void onCreate() {
        super.onCreate();
        mPrefs = Prefs.with(this);
        //AppEventsLogger.activateApp(this);
    }
}
