package com.godelivery.customer.locations;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.godelivery.R;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;

public class LocationBaseActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, LocationListener, OnLocationUpdatedListener {

    private static final int CODE_REQUEST_LOCATION = 1;
    private static final int REQUEST_CHECK_SETTINGS = 2;
    protected LatLng latLongCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest locationRequest;
    private OnLocationUpdatedListener locationUpdatedListener;
    private PermissionDialog permissionDialog;
    private boolean isPermissionOk;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLocationUpdateListener(this);
        setGoogleApiClient();
    }

    private void setGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(LocationBaseActivity.this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkForLocationPermissions();
    }


    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        setCurrentLocation();
        //stopLocationUpdates();
    }

    private void checkForLocationPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission_group.LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    CODE_REQUEST_LOCATION);
        } else
            displayLocationSettingsRequest();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CODE_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayLocationSettingsRequest();
                } else if (permissions.length > 0) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[0])) {
                        permissionDialog = new PermissionDialog(this, getString(R.string.str_permission_location), false, new PermissionDialog.OnAllowClickListener() {
                            @Override
                            public void onAllowClicked() {
                                checkForLocationPermissions();
                            }
                        });
                    } else {
                        permissionDialog = new PermissionDialog(this, getString(R.string.str_permission_location) + " "
                                + getString(R.string.str_open_setting_location), true, new PermissionDialog.OnAllowClickListener() {
                            @Override
                            public void onAllowClicked() {
                                openAppSettings();
                            }
                        });

                    }
                }
            }
        }
    }

    public void openAppSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_CHECK_SETTINGS);
    }

    private void displayLocationSettingsRequest() {

        // Create location request
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(30000);
        locationRequest.setFastestInterval(20000 / 2);
        locationRequest.setSmallestDisplacement(0.25f);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        // Create request to change gps settings
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        isPermissionOk = true;
                        setCurrentLocation();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(LocationBaseActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CHECK_SETTINGS) {
                displayLocationSettingsRequest();
                //setCurrentLocation();
                // startLocationUpdates();
            } else {
                locationUpdatedListener.onLocationUpdateFailure(new LatLng(0, 0));
            }
        }
    }


    private void setCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, R.string.msg_permission_not_granted, Toast.LENGTH_SHORT).show();
        } else {
            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                latLongCurrentLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                locationUpdatedListener.onLocationUpdated(latLongCurrentLocation);
            } else {
                //ProgressBarDialog.showProgressBar(this);
                //  Toast.makeText(this, R.string.msg_getting_location, Toast.LENGTH_SHORT).show();
                startLocationUpdates();
            }
        }
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, LocationBaseActivity.this);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, LocationBaseActivity.this);
    }

    public void setLocationUpdateListener(OnLocationUpdatedListener locationUpdatedListener) {
        this.locationUpdatedListener = locationUpdatedListener;
    }


    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();

        }
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();

        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected())
                startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected())
                stopLocationUpdates();
        }
        if (permissionDialog != null && permissionDialog.isShowing())
            permissionDialog.dismiss();
    }


    @Override
    public void onLocationUpdated(LatLng latLongCurrentLocation) {
        //  ProgressBarDialog.dismissProgressDialog();
    }

    @Override
    public void onLocationUpdateFailure(LatLng latLongCurrentLocation) {
        // Toast.makeText(this, getString(R.string.msg_location_err), Toast.LENGTH_SHORT).show();
    }

}