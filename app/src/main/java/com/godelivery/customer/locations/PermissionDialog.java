package com.godelivery.customer.locations;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.godelivery.R;


public class PermissionDialog extends Dialog {

    private OnAllowClickListener onAllowClickListener;
    private String message;
    private boolean showSettings;

    public PermissionDialog(Activity context, String message, boolean showSettings, OnAllowClickListener onAllowClickListener) {
        super(context);
        this.message = message;
        this.showSettings = showSettings;
        this.onAllowClickListener = onAllowClickListener;
        show();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_common_alert);
        setCancelable(true);
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        TextView tvAllow = (TextView) findViewById(R.id.tvPositive);
        tvMessage.setText(message);
        tvAllow.setText(getContext().getString(R.string.str_allow));
        if (showSettings)
            tvAllow.setText(getContext().getString(R.string.str_settings));
        tvAllow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onAllowClickListener.onAllowClicked();
                dismiss();
            }
        });
    }


    public interface OnAllowClickListener {
        void onAllowClicked();
    }
}
