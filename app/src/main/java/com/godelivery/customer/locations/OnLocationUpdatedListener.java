package com.godelivery.customer.locations;

import com.google.android.gms.maps.model.LatLng;

/*
 * Created by cbl96 on 15/12/16.
 */

public interface OnLocationUpdatedListener {
    void onLocationUpdated(LatLng latLongCurrentLocation);

    void onLocationUpdateFailure(LatLng latLongCurrentLocation);
}