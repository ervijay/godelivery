package com.godelivery.customer.activities;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.godelivery.R;
import com.godelivery.customer.locations.LocationBaseActivity;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GPSTracker;
import com.godelivery.databinding.ActivitySplashBinding;
import com.google.android.gms.maps.model.LatLng;

public class SplashActivity extends LocationBaseActivity {
    private Handler handler = null;
    private Runnable runnable = null;
    private ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        init();
         new GPSTracker(getApplicationContext());

        setAnimationOnImageView();
        setRunnable();
    }

    private void init() {
        handler = new Handler();
    }

    private void setRunnable() {
        runnable = new Runnable() {
            @Override
            public void run() {
                if (!AppController.mPrefs.getString(Constants.ACCESS_TOKEN, "").isEmpty()) {
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginSignUpActivity.class));
                }
                finish();
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        };
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);
        super.onPause();
    }

    @Override
    protected void onResume() {
        handler.postDelayed(runnable, Constants.SPLASH_DURATION);
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        if (handler != null && runnable != null) {
            handler.removeCallbacks(runnable);
            handler = null;
            runnable = null;
        }
        super.onDestroy();
    }

    private void setAnimationOnImageView() {
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        binding.ivSplashLogo.clearAnimation();
        binding.ivSplashLogo.startAnimation(anim);
    }

    @Override
    public void onLocationUpdated(LatLng latLongCurrentLocation) {
        AppController.mPrefs.save(Constants.LATITUDE, latLongCurrentLocation.latitude + "");
        AppController.mPrefs.save(Constants.LONGITUDE, latLongCurrentLocation.longitude + "");

    }
}

