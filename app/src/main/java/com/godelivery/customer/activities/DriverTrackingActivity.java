package com.godelivery.customer.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.fragments.dialog.DriverDetailsDialogFragment;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.Prefs;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonListPojo;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.customer.webservices.pojo.MapDirectionsPojo;
import com.godelivery.customer.webservices.pojo.MapGeoPoints;
import com.godelivery.customer.webservices.pojo.MapStepsData;
import com.godelivery.customer.webservices.pojo.MapsItemData;
import com.godelivery.databinding.ActivityDriverTrackingBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverTrackingActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener {
    private ActivityDriverTrackingBinding binding;
    private MapView mapView;
    private GoogleMap mGoogleMap;
    private String callDriver = "";
    private Data driverData;
    private LatLng latLngSource, latLngDriver;
    private String etstTime = "0";
    private String etstDistance = "0";
    private int count = 0, target = 0;
    String temptime="";
    private Marker marker;
    private Timer timer;
    private TimerTask timerTask;
    private BroadcastReceiver receiverRideStarted = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            binding.includedDriver.llCallCancel.setVisibility(View.GONE);
            Prefs.with(DriverTrackingActivity.this).save("isTripStarted", true);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_driver_tracking);
        mapView = binding.mapView;
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        driverData = new Gson().fromJson(getIntent().getStringExtra("driverData"), Data.class);
        setListeners();
        setViews();
        Prefs.with(this).save("bConfirm", true);
        ImageView back=(ImageView)findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void startTimerTask() {
        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                getRealTimeDriverData();
            }
        };

        timer.schedule(timerTask, 1000, 10000);
    }

    private void setViews() {
        GeneralFunctions.setRoundImage(this, driverData.fleet_thumb_image, binding.includedDriver.ivProfilePic, R.drawable.ic_profile);
        binding.includedDriver.tvDriverName.setText(driverData.fleet_name);
        binding.includedDriver.tvVehicleType.setText(Prefs.with(this).getString("vehicle", ""));
        callDriver = driverData.fleet_phone;
        LatLng latLng=new LatLng(Double.parseDouble(driverData.job_latitude),Double.parseDouble(driverData.job_longitude));
        LatLng dlatLng=new LatLng(Double.parseDouble(driverData.job_pickup_latitude),Double.parseDouble(driverData.job_pickup_longitude));
        float dis=distanceBtwPoint(latLng,dlatLng);
        double value=Math.round((dis/1000)*0.621371);
        binding.includedDriver.tvMiles.setText(value+" " + getString(R.string.miles));
    }
    private float distanceBtwPoint(LatLng current, LatLng destination) {


        Location loc1 = new Location("");
        loc1.setLatitude(current.latitude);
        loc1.setLongitude(current.longitude);

        Location loc2 = new Location("");
        loc2.setLatitude(destination.latitude);
        loc2.setLongitude(destination.longitude);
        float distanceInMeters = loc1.distanceTo(loc2);

        return distanceInMeters;


    }
    private void setListeners() {
        binding.includedDriver.topConstraint.setOnClickListener(this);
        binding.includedDriver.tvCancel.setOnClickListener(this);
        binding.includedDriver.tvCall.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.topConstraint:
                DriverDetailsDialogFragment driverDetailsDialogFragment = new DriverDetailsDialogFragment();
                Bundle bundle = new Bundle();
                bundle.putString("driverData", getIntent().getStringExtra("driverData"));
                driverDetailsDialogFragment.setArguments(bundle);
                driverDetailsDialogFragment.show(getSupportFragmentManager(), DriverDetailsDialogFragment.class.getName());
                //open driver details
                break;
            case R.id.tvCancel:
                cancelTask();
                break;
            case R.id.tvCall:
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", callDriver, null));
                startActivity(intent);
                break;
        }

    }

    private void makePath(final GoogleMap googleMap) {
        String origin = driverData.job_latitude + "," + driverData.job_longitude;
        String destination = driverData.job_pickup_latitude + "," + driverData.job_pickup_longitude;
        RestClient.get().getPath(origin, destination, "AIzaSyBbQGK4XowBRcKgkvTXHmkGZBghx5HfsO4")
                .enqueue(new Callback<MapDirectionsPojo>() {
                    @Override
                    public void onResponse(Call<MapDirectionsPojo> call, Response<MapDirectionsPojo> response) {
                        if (response.isSuccessful() && response.body().status.equals(getString(R.string.oks))) {
                            //etstTime=response.body().routes.get(0).duration.text;
                            //etstDistance=response.body().routes.get(0).distance.text;
                            drawPolyline(getDirectionPolylines(response.body().routes.get(0)), googleMap);
                        }
                    }

                    @Override
                    public void onFailure(Call<MapDirectionsPojo> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void getEstTimeAndDistance() {
        String origin = latLngDriver.latitude + "," + latLngDriver.longitude;
        String destination = driverData.job_pickup_latitude + "," + driverData.job_pickup_longitude;
        RestClient.get().getPath(origin, destination, "AIzaSyBbQGK4XowBRcKgkvTXHmkGZBghx5HfsO4")
                .enqueue(new Callback<MapDirectionsPojo>() {
                    @Override
                    public void onResponse(Call<MapDirectionsPojo> call, Response<MapDirectionsPojo> response) {
                        if (response.isSuccessful() && response.body().status.equals(getString(R.string.oks))) {
                            etstTime = response.body().routes.get(0).duration.text;
                            etstDistance = response.body().routes.get(0).distance.text;
                            binding.includedDriver.tvEta.setText(getString(R.string.eta)+" " + etstTime);
                            binding.includedDriver.tvMiles.setText(etstDistance);
                            if (target == 0 && Prefs.with(DriverTrackingActivity.this).getBoolean("isTripStarted", false)) {
                                drawPolylineForTripStarted(getDirectionPolylines(response.body().routes.get(0)));
                                target = -1;
                            } else if (target == -1) {
                                marker.setPosition(latLngDriver);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MapDirectionsPojo> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private ArrayList<LatLng> getDirectionPolylines(MapGeoPoints mapGeoPoints) {
        ArrayList<LatLng> directionList = new ArrayList<>();
        int duration = 0;

        MapGeoPoints leg = mapGeoPoints.legs.get(0);
        List<MapStepsData> steps = leg.steps;
        for (MapStepsData step : steps) {
            MapsItemData polyline = step.polyline;
            String points = polyline.points;
            duration=duration+Integer.parseInt(step.duration.text.split(" ")[0]);
            List<LatLng> singlePolyline = decodePoly(points);
            for (LatLng direction : singlePolyline) {
                directionList.add(direction);
            }
        }
        int days = duration / 86400;
        int hours = (duration - days * 86400) / 3600;
        int minutes = (duration - days * 86400 - hours * 3600) / 60;
        int seconds = duration - days * 86400 - hours * 3600 - minutes * 60;
        temptime = days +" "+ getString(R.string.days)+" " + hours +" "+ getString(R.string.hours) +" "+ minutes
               +" " + getString(R.string.mins)+" " + seconds +" "+ getString(R.string.seconds);
        if (days == 0) {
            temptime = hours +" "+ getString(R.string.hours) +" "+ minutes +" " + getString(R.string.mins)+" " + seconds + " seconds";

        }
        if (hours == 0) {
            temptime = minutes +" " + getString(R.string.mins)+" " + seconds +" "+ getString(R.string.seconds);
        }
        if (minutes == 0) {
            temptime = seconds +" "+ getString(R.string.seconds);
        }
        binding.includedDriver.tvEta.setText(getString(R.string.etas)+temptime);
        return directionList;
    }

    private void drawPolyline(ArrayList<LatLng> points, final GoogleMap googleMap) {

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.layout_marker, null);
        TextView textView = v.findViewById(R.id.tvTitle);
        ImageView imageView = v.findViewById(R.id.ivMarker);

        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(Color.RED);
        LatLng latLngSource = new LatLng(Double.valueOf(driverData.job_pickup_latitude), Double.valueOf(driverData.job_pickup_longitude));
        LatLng latLngDest = new LatLng(Double.valueOf(driverData.job_latitude), Double.valueOf(driverData.job_longitude));

        textView.setText(driverData.job_pickup_address);
        imageView.setImageResource(R.drawable.ic_your_location);
        googleMap.addMarker(new MarkerOptions().position(latLngSource)
                .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(v))));

        textView.setText(driverData.job_address);
        imageView.setImageResource(R.drawable.ic_location_1);

        googleMap.addMarker(new MarkerOptions().position(latLngDest)
                .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(v))));

        /*  googleMap.addMarker(new MarkerOptions().position(latLngSource)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_your_location)));
        googleMap.addMarker(new MarkerOptions().position(latLngDest)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_1)));*/
        LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();
        latLngBounds.include(latLngSource);
        latLngBounds.include(latLngDest);
        int width = getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (width * 0.20);
        googleMap.addPolyline(lineOptions);

        final LatLngBounds.Builder boundsBuilder;

        boundsBuilder = new LatLngBounds.Builder();

        boundsBuilder.include(latLngSource);
        boundsBuilder.include(latLngDest);


        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds bounds = boundsBuilder.build();
                googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 180), 1000, null);
            }
        });
    }

    public Bitmap loadBitmapFromView(View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(150,
                DrawerLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels,
                displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    private void drawPolylineForTripStarted(ArrayList<LatLng> points) {

        mGoogleMap.clear();
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(Color.RED);
        LatLng latLngSource = new LatLng(Double.valueOf(driverData.job_pickup_latitude), Double.valueOf(driverData.job_pickup_longitude));
        LatLng latLngDest = new LatLng(Double.valueOf(driverData.job_latitude), Double.valueOf(driverData.job_longitude));
        marker = mGoogleMap.addMarker(new MarkerOptions().position(latLngSource)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_top)));
        mGoogleMap.addMarker(new MarkerOptions().position(latLngDest)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_1)));
        LatLngBounds.Builder latLngBounds = new LatLngBounds.Builder();
        latLngBounds.include(latLngSource);
        latLngBounds.include(latLngDest);
        int width = getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (width * 0.10);
        mGoogleMap.addPolyline(lineOptions);
      //  googleMap.addPolyline(lineOptions);

        final LatLngBounds.Builder boundsBuilder;

        boundsBuilder = new LatLngBounds.Builder();

        boundsBuilder.include(latLngSource);
        boundsBuilder.include(latLngDest);


        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                LatLngBounds bounds = boundsBuilder.build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 180), 1000, null);
            }
        });

    }

    private ArrayList<LatLng> decodePoly(String encoded) {

        ArrayList<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    private void cancelTask() {
        if (GeneralFunctions.isConnectedToNetwork(this, true)) {
            GeneralFunctions.showDialog(this);
            HashMap<String, String> hashMap = new HashMap<String, String>();
            hashMap.put("access_token", Prefs.with(this).getString(Constants.ACCESS_TOKEN, ""));
            hashMap.put("job_id", driverData.job_hash);
            hashMap.put("vendor_id", driverData.fleet_id);
            //TODO
            hashMap.put("job_status", "9");
            hashMap.put("domain_name", "prueba11jun.godelivery.com.do");
            RestClient.get().cancelTask(hashMap).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    GeneralFunctions.dismissDialog();
                    if (response.isSuccessful()) {
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(DriverTrackingActivity.this, getString(R.string.api_failure_error), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        makePath(googleMap);
        startTimerTask();
    }

    private void getRealTimeDriverData() {
        if (GeneralFunctions.isConnectedToNetwork(this, true)) {
            RestClient.get().getDriverTrack(driverData.job_hash).enqueue(new Callback<CommonListPojo>() {
                @Override
                public void onResponse(Call<CommonListPojo> call, Response<CommonListPojo> response) {
                    if (response.isSuccessful()) {
                        latLngDriver = new LatLng(
                                Double.valueOf(response.body().data.get(0).fleet_latitude),
                                Double.valueOf(response.body().data.get(0).fleet_longitude));
                        if (!Prefs.with(DriverTrackingActivity.this).getBoolean("isTripStarted", false)) {
                            if (count == 0) {
                                count++;
                                marker = mGoogleMap.addMarker(new MarkerOptions().position(latLngDriver)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_car_top)));
                            } else {
                                marker.setPosition(latLngDriver);
                            }
                        }
                        getEstTimeAndDistance();
                    }
                }

                @Override
                public void onFailure(Call<CommonListPojo> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if (timer != null) {
            timerTask.cancel();
            timer.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
