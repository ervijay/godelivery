package com.godelivery.customer.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.fragments.dialog.HistoryDialogFragment;
import com.godelivery.customer.fragments.dialog.SavedLocationsDialogFragment;
import com.godelivery.customer.fragments.dialog.UpdateProfileDialogFragment;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GPSTracker;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.MapInterface;
import com.godelivery.customer.utils.Prefs;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.databinding.ActivityHomeBinding;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ActivityHomeBinding binding;
    private MapInterface mapInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        new GPSTracker(this);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_side_menu);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleDrawer();
            }
        });
        toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        binding.tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openLogoutAlertDialog();
            }
        });
        navigationView.setItemIconTintList(null);
        setViews();
        //checkBookingExist();
        Log.e("token", Prefs.with(this).getString(Constants.ACCESS_TOKEN, ""));

    }

    public void setInterface(MapInterface interfac) {
        this.mapInterface = interfac;
    }


    private void checkBookingExist() {
        if (!Prefs.with(this).getString("driverData", "").isEmpty()) {
            startActivity(new Intent(HomeActivity.this, DriverTrackingActivity.class).putExtra("driverData",
                    Prefs.with(this).getString("driverData", "")));
        }
    }

    private void setViews() {
        Data data = AppController.mPrefs.getObject(Constants.PROFILE_DATA, Data.class);
        View headerLayout = navigationView.getHeaderView(0);
        TextView tvUserName = headerLayout.findViewById(R.id.tvUserName);
        TextView tvPhoneNumber = headerLayout.findViewById(R.id.tvPhoneNumber);
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpdateProfileDialogFragment updateProfileDialogFragment = new UpdateProfileDialogFragment();
                updateProfileDialogFragment.show(getSupportFragmentManager(), UpdateProfileDialogFragment.class.getName());
            }
        });
        tvUserName.setText(data.vendor_details.first_name);
        tvPhoneNumber.setText(data.vendor_details.phone_no);

    }

    public void toggleDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_history) {
            HistoryDialogFragment historyDialogFragment = new HistoryDialogFragment();
            historyDialogFragment.show(getSupportFragmentManager(), HistoryDialogFragment.class.getName());
        } else if (id == R.id.nav_payment) {
        } /*else if (id == R.id.nav_setting) {
            SettingsDialogFragment settingsDialogFragment=new SettingsDialogFragment();
            settingsDialogFragment.show(getSupportFragmentManager(),SettingsDialogFragment.class.getName());
        } */ else if (id == R.id.nav_saved_location) {
            SavedLocationsDialogFragment savedLocationsDialogFragment = new SavedLocationsDialogFragment();
            savedLocationsDialogFragment.show(getSupportFragmentManager(), SavedLocationsDialogFragment.class.getName());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openLogoutAlertDialog() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.log_out_alert)
                .setCancelable(false)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        apiLogout();

                    }
                })
                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

    private void apiLogout() {
        if (GeneralFunctions.isConnectedToNetwork(this, true)) {
            GeneralFunctions.showDialog(this);
            RestClient.get().logout("envios.godelivery.com.do").enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (response.isSuccessful()) {
                        startActivity(new Intent(HomeActivity.this, LoginSignUpActivity.class));
                        AppController.mPrefs.removeAll();
                        finish();
                    } else {
                        Toast.makeText(HomeActivity.this, getString(R.string.api_failure_error), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(HomeActivity.this, getString(R.string.api_failure_error), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }



}
