package com.godelivery.customer.activities;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.godelivery.R;
import com.godelivery.customer.adapters.VehiclesAdapter;
import com.godelivery.customer.fragments.dialog.MaxWeightDialogFragment;
import com.godelivery.customer.utils.AppController;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;
import com.godelivery.customer.utils.MapInterface;
import com.godelivery.customer.utils.Prefs;
import com.godelivery.customer.webservices.RestClient;
import com.godelivery.customer.webservices.pojo.BookingModel;
import com.godelivery.customer.webservices.pojo.BookingPickupDropoff;
import com.godelivery.customer.webservices.pojo.BookingUserModel;
import com.godelivery.customer.webservices.pojo.CommonListPojo;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Data;
import com.godelivery.customer.webservices.pojo.DropOrSignupData;
import com.godelivery.customer.webservices.pojo.Dropoff;
import com.godelivery.customer.webservices.pojo.MapDirectionsPojo;
import com.godelivery.customer.webservices.pojo.MapGeoPoints;
import com.godelivery.customer.webservices.pojo.MapStepsData;
import com.godelivery.customer.webservices.pojo.MapsItemData;
import com.godelivery.customer.webservices.pojo.PickupDropOffModel;
import com.godelivery.customer.webservices.pojo.SizeModel;
import com.godelivery.customer.webservices.pojo.TaskDetails;
import com.godelivery.customer.webservices.pojo.VehiclePojo;
import com.godelivery.databinding.ActivityTrackingBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingActivity extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener,MapInterface {
    private ActivityTrackingBinding binding;
    private MapView mapView;
    private PickupDropOffModel pickUpData;
    private PickupDropOffModel dropOfData;
    public static List<Data> vehicleList = new ArrayList<>();
    private int count = 0, price = 0;
    private AlertDialog alertDialogConfirm;
    public static VehiclesAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tracking);
        mapView = binding.mapView;
        vehicleList.clear();
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        setListeners();
        ImageView iv=(ImageView)findViewById(R.id.back);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getVehiclesList() {
        if (GeneralFunctions.isConnectedToNetwork(this, true)) {
            GeneralFunctions.showDialog(this);
            RestClient.get().getVehicles().enqueue(new Callback<VehiclePojo>() {
                @Override
                public void onResponse(Call<VehiclePojo> call, Response<VehiclePojo> response) {
                    if (response.isSuccessful()) {
                        vehicleList = response.body().data;
                        //setAdapter(vehicleList);
                        apiGetCost();
                    }
                }

                @Override
                public void onFailure(Call<VehiclePojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(BookingActivity.this, getString(R.string.api_failure_error), Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    private void apiGetCost() {
        if (GeneralFunctions.isConnectedToNetwork(this, true)) {
            List<SizeModel> listItems = new ArrayList<>();
            listItems.add(new SizeModel());
            Dropoff dropoff = new Dropoff();
            dropoff.access_token = AppController.mPrefs.getString(Constants.ACCESS_TOKEN, "");
            dropoff.pickup = new DropOrSignupData(pickUpData.lat, pickUpData.lng);
            dropoff.delivery = new DropOrSignupData(dropOfData.lat, dropOfData.lng);
            dropoff.vehicle = count + 1;
            dropoff.items = listItems;

            RestClient.get().getCost(dropoff).enqueue(new Callback<CommonPojo>() {
                @Override
                public void onResponse(Call<CommonPojo> call, Response<CommonPojo> response) {
                    if (count == vehicleList.size() - 1) {
                        GeneralFunctions.dismissDialog();
                    }
                    if (response.isSuccessful()) {
                        vehicleList.get(count).price = response.body().data.price;
                        if (count != vehicleList.size() - 1) {
                            count++;
                            apiGetCost();

                        } else {
                            for (Data data : vehicleList) {
                                if (data.price == 0)
                                    price++;
                            }
                            if (price == vehicleList.size()) {
                                openConfimDialog(getString(R.string.no_vehicle_list));
                            } else {
                                binding.llVehicleType.setVisibility(View.VISIBLE);
                                setAdapter(vehicleList);
                            }
                        }

                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                           // Toast.makeText(BookingActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            /*startActivity(new Intent(BookingActivity.this,HomeActivity.class));
                            finishAffinity();*/
                            openConfimDialog(getString(R.string.address_string));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonPojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(BookingActivity.this, getString(R.string.api_failure_error), Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    private void setAdapter(List<Data> data) {
        final LinearLayoutManager layoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false);

        binding.rvVehicles.setLayoutManager(layoutManager);
        mAdapter=new VehiclesAdapter(this, data);
        binding.rvVehicles.setAdapter(mAdapter);
        final ImageView left=(ImageView)findViewById(R.id.left);
        final ImageView right=(ImageView)findViewById(R.id.right);
        binding.rvVehicles.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (layoutManager.findFirstCompletelyVisibleItemPosition() == 0) {
                    left.setVisibility(View.GONE);
                    right.setVisibility(View.VISIBLE);
                }
                if(layoutManager.findLastCompletelyVisibleItemPosition()==vehicleList.size()-1)
                {
                    left.setVisibility(View.VISIBLE);
                    right.setVisibility(View.GONE);
                }

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                return true;
            }
        });
        getVehiclesList();
        pickUpData = new Gson().fromJson(getIntent().getStringExtra("pickUpData"), PickupDropOffModel.class);
        dropOfData = new Gson().fromJson(getIntent().getStringExtra("dropOffData"), PickupDropOffModel.class);
        addSourceAndDestMarker(googleMap);
    }


    private void addSourceAndDestMarker(GoogleMap googleMap) {
        makePath(googleMap);
    }

    private void setListeners() {
        binding.btnBook.setOnClickListener(this);
        binding.btnMaxWt.setOnClickListener(this);
    }

    private void makePath(final GoogleMap googleMap) {
        String origin = Double.parseDouble(pickUpData.lat) + "," + Double.parseDouble(pickUpData.lng);
        String destination = Double.parseDouble(dropOfData.lat) + "," + Double.parseDouble(dropOfData.lng);
        RestClient.get().getPath(origin, destination, "AIzaSyBbQGK4XowBRcKgkvTXHmkGZBghx5HfsO4")
                .enqueue(new Callback<MapDirectionsPojo>() {
                    @Override
                    public void onResponse(Call<MapDirectionsPojo> call, Response<MapDirectionsPojo> response) {
                        if (response.isSuccessful() && response.body().status.equals(getString(R.string.ok))) {
                            drawPolyline(getDirectionPolylines(response.body().routes.get(0)), googleMap);
                        }
                    }

                    @Override
                    public void onFailure(Call<MapDirectionsPojo> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private ArrayList<LatLng> getDirectionPolylines(MapGeoPoints mapGeoPoints) {
        ArrayList<LatLng> directionList = new ArrayList<>();
        MapGeoPoints leg = mapGeoPoints.legs.get(0);
        List<MapStepsData> steps = leg.steps;
        for (MapStepsData step : steps) {
            MapsItemData polyline = step.polyline;
            String points = polyline.points;
            List<LatLng> singlePolyline = decodePoly(points);
            for (LatLng direction : singlePolyline) {
                directionList.add(direction);
            }
        }
        return directionList;
    }

    private void drawPolyline(ArrayList<LatLng> points, final GoogleMap googleMap) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.layout_marker, null);
        TextView textView = v.findViewById(R.id.tvTitle);
        ImageView imageView = v.findViewById(R.id.ivMarker);

        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(Color.RED);
        LatLng latLngSource = new LatLng(Double.parseDouble(pickUpData.lat), Double.parseDouble(pickUpData.lng));
        LatLng latLngDest = new LatLng(Double.parseDouble(dropOfData.lat), Double.parseDouble(dropOfData.lng));

        textView.setText(pickUpData.location);
        imageView.setImageResource(R.drawable.ic_your_location);

        googleMap.addMarker(new MarkerOptions().position(latLngSource)
                .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(v))));

        textView.setText(dropOfData.location);
        imageView.setImageResource(R.drawable.ic_location_1);

        googleMap.addMarker(new MarkerOptions().position(latLngDest)
                .icon(BitmapDescriptorFactory.fromBitmap(loadBitmapFromView(v))));

        googleMap.addPolyline(lineOptions);

         final LatLngBounds.Builder boundsBuilder;

        boundsBuilder = new LatLngBounds.Builder();

        boundsBuilder.include(latLngSource);
        boundsBuilder.include(latLngDest);


        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                    LatLngBounds bounds = boundsBuilder.build();
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 180), 1000, null);
            }
        });

  }

    public Bitmap loadBitmapFromView(View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay()
                .getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(100,
                DrawerLayout.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels,
                displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }


    private ArrayList<LatLng> decodePoly(String encoded) {

        ArrayList<LatLng> poly = new ArrayList<>();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBook:
                if (vehicleList != null) {
                    for (Data data : vehicleList) {
                        if (data.isPressed)
                            openSuccessDialog(data);
                    }
                }
                 else {
                    Toast.makeText(this, R.string.select_vehicle, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btnMaxWt:
                MaxWeightDialogFragment maxWeightDialogFragment = new MaxWeightDialogFragment();
                maxWeightDialogFragment.show(getSupportFragmentManager(), MaxWeightDialogFragment.class.getName());
                break;
        }

    }

    private void openSuccessDialog(final Data data) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View alertView = View.inflate(this, R.layout.layout_confirm_dialog, null);
        TextView tvSourceName = alertView.findViewById(R.id.tvSourceName);
        tvSourceName.setText(pickUpData.location);

        TextView tvSourceDate = alertView.findViewById(R.id.tvSourceDate);
        tvSourceDate.setText(pickUpData.pickUpTime);

        TextView tvDestName = alertView.findViewById(R.id.tvDestName);
        tvDestName.setText(dropOfData.location);


        TextView tvDestDate = alertView.findViewById(R.id.tvDestDate);
        tvDestDate.setText(dropOfData.pickUpTime);

        TextView tvVehicle = alertView.findViewById(R.id.tvVehicle);
        tvVehicle.setText(data.code);

        TextView tvBaseFare = alertView.findViewById(R.id.tvBaseFare);
        tvBaseFare.setText("$" + data.price);

        TextView btnConfirm = alertView.findViewById(R.id.btnConfirm);
        TextView btnCancel = alertView.findViewById(R.id.btnCancel);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Prefs.with(BookingActivity.this).save("price", String.valueOf(data.price));
                Prefs.with(BookingActivity.this).save("vehicle", data.code);
                Prefs.with(BookingActivity.this).save("pickUpTime", pickUpData.pickUpTime);
                Prefs.with(BookingActivity.this).save("pickUpLocation", pickUpData.location);
                Prefs.with(BookingActivity.this).save("dropOffLocation", dropOfData.location);
                Prefs.with(BookingActivity.this).save("dropOffTime", dropOfData.pickUpTime);
                apiConfirmBooking();
                alertDialogConfirm.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogConfirm.dismiss();
            }
        });
        alertBuilder.setView(alertView);
        alertBuilder.setCancelable(false);
        alertDialogConfirm = alertBuilder.create();
        alertDialogConfirm.show();
    }


    private void apiConfirmBooking() {
        if (GeneralFunctions.isConnectedToNetwork(this, true)) {
            GeneralFunctions.showDialog(this);
            BookingPickupDropoff bookingPickup = new BookingPickupDropoff();
            bookingPickup.address = pickUpData.location;
            bookingPickup.latitude = pickUpData.lat;
            bookingPickup.longitude = pickUpData.lng;
            bookingPickup.datetime = pickUpData.pickUpTime;
            bookingPickup.customer = new BookingUserModel(pickUpData.email, pickUpData.countryCode + pickUpData.phoneNumber, pickUpData.name);

            BookingPickupDropoff bookingdropOff = new BookingPickupDropoff();
            bookingdropOff.address = dropOfData.location;
            bookingdropOff.latitude = dropOfData.lat;
            bookingdropOff.longitude = dropOfData.lng;
            bookingdropOff.datetime = dropOfData.pickUpTime;
            bookingdropOff.customer = new BookingUserModel(dropOfData.email, dropOfData.countryCode + dropOfData.phoneNumber, dropOfData.name);
            List<SizeModel> listItems = new ArrayList<>();
            listItems.add(new SizeModel());
            BookingModel bookingModel = new BookingModel();
            bookingModel.access_token = AppController.mPrefs.getString(Constants.ACCESS_TOKEN, "");
            bookingModel.order_id = String.valueOf(System.currentTimeMillis());
            bookingModel.pickup = bookingPickup;
            bookingModel.items = listItems;
            bookingModel.delivery = bookingdropOff;
            RestClient.get().addBooking(bookingModel).enqueue(new Callback<CommonListPojo>() {
                @Override
                public void onResponse(Call<CommonListPojo> call, Response<CommonListPojo> response) {
                    GeneralFunctions.dismissDialog();
                    if (response.isSuccessful()) {
                        openConfirmDialog(response.body());
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.errorBody().string());
                            Toast.makeText(BookingActivity.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                }

                @Override
                public void onFailure(Call<CommonListPojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(BookingActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void openConfimDialog(String message) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View alertView = View.inflate(this, R.layout.layout_booking_confirm, null);
        TextView tvConfirm = alertView.findViewById(R.id.tvOkay);
        TextView tvCancel = alertView.findViewById(R.id.tvSourceName);
        tvCancel.setText(message);
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });
        alertBuilder.setView(alertView);
        alertBuilder.setCancelable(false);
        alertDialogConfirm = alertBuilder.create();
        alertDialogConfirm.show();
    }

    private void openConfirmDialog(final CommonListPojo body) {
        final AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
        View alertView = View.inflate(this, R.layout.layout_booking_confirm, null);
        TextView tvConfirm = alertView.findViewById(R.id.tvOkay);
        tvConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(body.data.get(0).task.job_hash!=null){
                    hitApiVendorSearch(body.data.get(0).task);

                }else{
                    Toast.makeText(BookingActivity.this, R.string.notification_driver_is_assign, Toast.LENGTH_LONG).show();
                    alertDialogConfirm.dismiss();
                    finish();
                }

            }
        });
        alertBuilder.setView(alertView);
        alertBuilder.setCancelable(false);
        alertDialogConfirm = alertBuilder.create();
        alertDialogConfirm.show();
    }

    private void hitApiVendorSearch(TaskDetails task) {
        if (GeneralFunctions.isConnectedToNetwork(this, true)) {
            GeneralFunctions.showDialog(this);
            RestClient.get().searchDriver(task.job_hash).enqueue(new Callback<CommonListPojo>() {
                @Override
                public void onResponse(Call<CommonListPojo> call, Response<CommonListPojo> response) {
                    if (response.isSuccessful()) {
                        if (response.body().data != null && response.body().data.size() > 0) {
                            if (response.body().data.get(0).fleet_id == null) {
                                alertDialogConfirm.dismiss();
                                Toast.makeText(BookingActivity.this, R.string.notification_driver_is_assign, Toast.LENGTH_LONG).show();
                                startActivity(new Intent(BookingActivity.this, HomeActivity.class));
                                finish();
                            } else {
                                startActivity(new Intent(BookingActivity.this, DriverTrackingActivity.class).putExtra("driverData",
                                        new Gson().toJson(response.body().data.get(0))));
                                Prefs.with(BookingActivity.this).save("driverData", new Gson().toJson(response.body().data.get(0)));
                                alertDialogConfirm.dismiss();
                                finish();
                            }
                        } else {
                            Toast.makeText(BookingActivity.this, R.string.notification_driver_is_assign, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonListPojo> call, Throwable t) {
                    GeneralFunctions.dismissDialog();
                    t.printStackTrace();
                    Toast.makeText(BookingActivity.this, getString(R.string.api_failure_error), Toast.LENGTH_LONG).show();

                }
            });
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onData() {

    }




}
