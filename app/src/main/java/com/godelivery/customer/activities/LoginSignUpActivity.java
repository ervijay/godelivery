package com.godelivery.customer.activities;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.godelivery.R;
import com.godelivery.customer.fragments.LoginFragment;
import com.godelivery.customer.utils.Constants;
import com.godelivery.customer.utils.GeneralFunctions;

public class LoginSignUpActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, R.layout.activity_login_signup);
        GeneralFunctions.addFragmentWithAnimation(this, android.R.id.content, new LoginFragment(), false, false, Constants.SLIDE_ALL);
    }
}
