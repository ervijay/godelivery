package com.godelivery.customer.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.godelivery.R;
import com.godelivery.customer.fragments.dialog.SavedLocationsDialogFragment;
import com.godelivery.customer.webservices.pojo.FavLocationData;

import java.util.ArrayList;
import java.util.List;


public class SavedLocationsAdapter extends RecyclerView.Adapter<SavedLocationsAdapter.ViewHolder> {
    private List<FavLocationData> fav_locations = new ArrayList<>();
    private String from;
    private Context context;

    public SavedLocationsAdapter(Context context, List<FavLocationData> fav_locations, String from) {
        this.fav_locations = fav_locations;
        this.from = from;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_location_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvBoldAddress.setText(fav_locations.get(position).address.substring(fav_locations.get(position).address.indexOf("-") + 1,
                fav_locations.get(position).address.length()));
        try {
            String s = fav_locations.get(position).address.substring(0, fav_locations.get(position).address.indexOf("-"));
            Spannable spannable = new SpannableString(s);
            spannable.setSpan(new StyleSpan(Typeface.BOLD), 0, s.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            holder.tvAddress.setText(spannable);
        } catch (Exception e) {
            holder.tvAddress.setText(fav_locations.get(position).address);
        }
    }

    @Override
    public int getItemCount() {
        return fav_locations.size()>30?30:fav_locations.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvAddress, tvBoldAddress;

        ViewHolder(View itemView) {
            super(itemView);
            tvAddress = itemView.findViewById(R.id.tvSourceName);
            tvBoldAddress = itemView.findViewById(R.id.tvName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!from.isEmpty()) {
                        SavedLocationsDialogFragment savedLocationsDialogFragment = (SavedLocationsDialogFragment) ((AppCompatActivity) context).getSupportFragmentManager().
                                findFragmentByTag(SavedLocationsDialogFragment.class.getName());
                        savedLocationsDialogFragment.getTargetFragment().
                                onActivityResult(savedLocationsDialogFragment.getTargetRequestCode(), ((AppCompatActivity) context).RESULT_OK,
                                        new Intent().
                                                putExtra("lat",
                                                        Double.valueOf(fav_locations.get(getAdapterPosition()).latitude)).
                                                putExtra("lng",
                                                        Double.valueOf(fav_locations.get(getAdapterPosition()).longitude))
                                                .putExtra("address",
                                                        fav_locations.get(getAdapterPosition()).address));
                        savedLocationsDialogFragment.dismissAllowingStateLoss();
                    }
                }
            });
        }
    }
}
