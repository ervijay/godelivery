package com.godelivery.customer.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.godelivery.R;
import com.jsibbold.zoomage.ZoomageView;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.RoundedCornersTransformation;

public class ReferenceImageAdapter extends RecyclerView.Adapter<ReferenceImageAdapter.ViewHolder> {
    private List<String> imageList = new ArrayList<>();
    private String from;
    private Context context;
    private ScaleGestureDetector scaleGestureDetector;
    private Matrix matrix = new Matrix();
    public ReferenceImageAdapter(Context context, List<String> imageList) {
        this.imageList = imageList;
        this.context = context;
        //scaleGestureDetector = new ScaleGestureDetector(context,new ScaleListener());

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.layout_image_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Glide.with((Activity) context).load( imageList.get(position)).bitmapTransform(new RoundedCornersTransformation(context, 8, 2,
                RoundedCornersTransformation.CornerType.ALL)).diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_camera).crossFade().into(holder.ivImage);

        /*GeneralFunctions.setImageWithRoundedCorner((Activity) context,
                imageList.get(position), holder.ivImage, android.R.color.darker_gray);*/
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivImage, ivDelete;

        ViewHolder(View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.ivImage);
            ivDelete = itemView.findViewById(R.id.ivDelete);
            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageList.remove(getAdapterPosition());
                    notifyItemRemoved(getAdapterPosition());
                }
            });
            ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialog(imageList.get(getAdapterPosition()));
                }
            });
        }

    }

    private void showDialog(String image) {
        final Dialog picDialog = new Dialog(context);
        picDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        picDialog.setCancelable(true);
        picDialog.setCanceledOnTouchOutside(true);
        LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = li.inflate(R.layout.dialog_image
                , null);
        picDialog.setContentView(view);

        final ZoomageView ivDelete = (ZoomageView) view.findViewById(R.id.zoom);
        Glide.with(context).load(image).into(ivDelete);
        picDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        picDialog.show();
    }

  /*
    private class ScaleListener extends ScaleGestureDetector.
            SimpleOnScaleGestureListener {

        private ImageView imageView;
        public ScaleListener(ImageView image)
        {
            this.imageView=image;
        }
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            float scaleFactor = detector.getScaleFactor();
            scaleFactor = Math.max(0.1f, Math.min(scaleFactor, 5.0f));
            matrix.setScale(scaleFactor, scaleFactor);
            imageView.setImageMatrix(matrix);
            return true;
        }
    }*/
}
