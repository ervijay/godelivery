package com.godelivery.customer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.godelivery.R;
import com.godelivery.customer.fragments.dialog.DropOffDialogFragment;
import com.godelivery.customer.fragments.dialog.PickUpPointDialogFragment;
import com.godelivery.customer.webservices.pojo.Datum;

import java.util.ArrayList;


public class NameAdapter extends RecyclerView.Adapter<NameAdapter.ViewHolder> {
    private ArrayList<Datum> nameData = new ArrayList<>();
    private Context context;
    private boolean vallueue;

    public NameAdapter(Context context, ArrayList<Datum> nameData, boolean vallueue ) {
        this.context = context;
        this.nameData = nameData;
        this.vallueue= vallueue;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_name_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvSourceDate.setText(nameData.get(position).customer_username);
        holder.tvPhoneNumber.setText(nameData.get(position).customer_phone);

    }

    @Override
    public int getItemCount() {
        return nameData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSourceDate, tvPhoneNumber;
        private RelativeLayout rlayout;

        ViewHolder(View itemView) {
            super(itemView);
            tvSourceDate = itemView.findViewById(R.id.tvSourceDate);
            tvPhoneNumber = itemView.findViewById(R.id.tvPhoneNumber);
            rlayout =  itemView.findViewById(R.id.rlayout);

            rlayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(vallueue){
                        PickUpPointDialogFragment.passNameData.doSomething(nameData.get(getAdapterPosition()));
                    }else{
                        DropOffDialogFragment.passNameData.doSomething(nameData.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}
