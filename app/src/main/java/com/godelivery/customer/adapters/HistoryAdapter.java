package com.godelivery.customer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.godelivery.R;

import java.util.ArrayList;
import java.util.List;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private List<List<String>> historyData = new ArrayList<>();
    private Context context;

    public HistoryAdapter(Context context, List<List<String>> historyData) {
        this.context = context;
        this.historyData = historyData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_history_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvDropTime.setText(historyData.get(position).get(7));
        holder.tvPickupTime.setText(historyData.get(position).get(7));
        holder.tvDest.setText(historyData.get(position).get(6));
        holder.tvSource.setText(historyData.get(position).get(6));
//        holder.tvVehicleTypeHint.setText(historyData.get(position).);
//        holder.tvBaseFareHint.setText("");

    }

    @Override
    public int getItemCount() {
        return historyData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvSource, tvDest, tvPickupTime, tvDropTime, tvVehicleTypeHint, tvBaseFareHint;

        ViewHolder(View itemView) {
            super(itemView);
            tvDest = itemView.findViewById(R.id.tvDestName);
            tvDropTime = itemView.findViewById(R.id.tvDestDate);
            tvSource = itemView.findViewById(R.id.tvSourceName);
            tvPickupTime = itemView.findViewById(R.id.tvSourceDate);
            tvVehicleTypeHint = itemView.findViewById(R.id.tvVehicleTypeHint);
            tvBaseFareHint = itemView.findViewById(R.id.tvBaseFareHint);
        }
    }
}
