package com.godelivery.customer.adapters;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.godelivery.R;
import com.godelivery.customer.webservices.pojo.Data;

import java.util.ArrayList;
import java.util.List;

public class VehiclesAdapter extends RecyclerView.Adapter<VehiclesAdapter.ViewHolder> {
    private List<Data> data = new ArrayList<>();
    private Context context;
    private int position = -1;

    public VehiclesAdapter(Context context, List<Data> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_vehicle, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvVehicleName.setText(data.get(position).name);
        holder.tvCost.setText("$" + data.get(position).price);
        if(data.get(position).price==0)
        {
            holder.ivImage.setVisibility(View.GONE);
            holder.ivImagePress.setVisibility(View.GONE);
            holder.tvVehicleName.setVisibility(View.GONE);
            holder.tvCost.setVisibility(View.GONE);
        }
        else
        {
            holder.ivImage.setVisibility(View.VISIBLE);
            holder.ivImagePress.setVisibility(View.VISIBLE);
            holder.tvVehicleName.setVisibility(View.VISIBLE);
            holder.tvCost.setVisibility(View.VISIBLE);
        }
        if (data.get(position).isPressed) {
            holder.llImage.setVisibility(View.GONE);
            holder.llPress.setVisibility(View.VISIBLE);
            switch (data.get(position).code) {
                case "motorcycle":
                    holder.ivImagePress.setImageResource(R.drawable.ic_bike_color);
                    break;
                case "motorcycle-delivery-box":
                    holder.ivImagePress.setImageResource(R.drawable.ic_bike_color);
                    break;
                case "van":
                    holder.ivImagePress.setImageResource(R.drawable.ic_van_color);
                    break;
                case "truck":
                    holder.ivImagePress.setImageResource(R.drawable.ic_truck_color);
                    break;
                case "car":
                    holder.ivImagePress.setImageResource(R.drawable.ic_car_color);
                    break;
            }
        } else {
            holder.llPress.setVisibility(View.GONE);
            holder.llImage.setVisibility(View.VISIBLE);
            switch (data.get(position).code) {
                case "motorcycle":
                    holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_bike));
                    break;
                case "motorcycle-delivery-box":
                    holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_bike));
                    break;
                case "van":
                    holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_van));
                    break;
                case "truck":
                    holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_truck_b_w));
                    break;
                case "car":
                    holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_car));
                    break;
            }
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public Data getSelectedItem() {
        if (position != -1) {
            return data.get(position);
        } else {
            return null;
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvVehicleName, tvCost;
        private ImageView ivImage, ivImagePress;
        private LinearLayout llImage,llPress;

        ViewHolder(View itemView) {
            super(itemView);
            tvVehicleName = itemView.findViewById(R.id.tvVehicleName);
            tvCost = itemView.findViewById(R.id.tvCost);
            ivImage = itemView.findViewById(R.id.ivImage);
            ivImagePress = itemView.findViewById(R.id.ivImagePress);
            llImage = itemView.findViewById(R.id.llImage);
            llPress = itemView.findViewById(R.id.llPress);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() != -1) {
                        data.get(getAdapterPosition()).isPressed = false;
                    }
                    for(Data data:data)
                    {
                        data.isPressed=false;
                    }
                    data.get(getAdapterPosition()).isPressed = true;
                    position=getAdapterPosition();
                    notifyDataSetChanged();
                }

            });
        }
    }
}
