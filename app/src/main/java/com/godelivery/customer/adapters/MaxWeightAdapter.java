package com.godelivery.customer.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.godelivery.R;
import com.godelivery.customer.activities.BookingActivity;
import com.godelivery.customer.webservices.pojo.Data;

import java.util.ArrayList;
import java.util.List;

public class MaxWeightAdapter extends RecyclerView.Adapter<MaxWeightAdapter.ViewHolder> {
    private List<Data> data = new ArrayList<>();
    private Context context;
    private Dialog mDialog;
    public MaxWeightAdapter(Context context, List<Data> data, Dialog dialog) {
        this.data = data;
        this.context = context;
        this.mDialog=dialog;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_adapter_max_weight, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvVehicleName.setText(data.get(position).name);
        holder.tvCost.setText("$" + data.get(position).price);
        holder.tvVolume.setText(context.getString(R.string.max_vol)+" " + data.get(position).max_volume);
        if (data.get(position).isPressed) {
            holder.mView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
            holder.tvVehicleHName.setTextColor(ContextCompat.getColor(context,android.R.color.white));
            holder.tvHCost.setTextColor(ContextCompat.getColor(context,android.R.color.white));
        }
        else {
            holder.mView.setCardBackgroundColor(ContextCompat.getColor(context,android.R.color.white));
            holder.tvVehicleHName.setTextColor(ContextCompat.getColor(context,android.R.color.darker_gray));
            holder.tvHCost.setTextColor(ContextCompat.getColor(context,android.R.color.darker_gray));
        }
        switch (data.get(position).code) {
            case "motorcycle":
                holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rsz_1_bike));
                break;
            case "motorcycle-delivery-box":
                holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rsz_2_bike_with_box));
                break;
            case "van":
                holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rsz_4_van));
                break;
            case "truck":
                holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rsz_5_camion));
                break;
            case "car":
                holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.rsz_3_car));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvVehicleName, tvCost, tvVolume;
        private TextView tvVehicleHName, tvHCost;
        private ImageView ivImage;
        private CardView mView;

        ViewHolder(View itemView) {
            super(itemView);
            tvVehicleName = itemView.findViewById(R.id.tvVehicle);
            tvCost = itemView.findViewById(R.id.tvBaseFare);
            tvVehicleHName = itemView.findViewById(R.id.tvVehicleTypeHint);
            tvHCost = itemView.findViewById(R.id.tvBaseFareHint);
            tvVolume = itemView.findViewById(R.id.tvVehicleTypeHint);
            ivImage = itemView.findViewById(R.id.ivImage);
            mView=(CardView)itemView.findViewById(R.id.mainView);
            mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (getAdapterPosition() != -1) {
                        data.get(getAdapterPosition()).isPressed = false;
                    }
                    for(Data data:data)
                    {
                        data.isPressed=false;
                    }
                    data.get(getAdapterPosition()).isPressed = true;
                    notifyDataSetChanged();
                    BookingActivity.mAdapter.notifyDataSetChanged();
                    mDialog.dismiss();

                }

            });
        }
    }
}
