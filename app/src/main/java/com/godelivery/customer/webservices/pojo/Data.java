package com.godelivery.customer.webservices.pojo;

import java.util.ArrayList;
import java.util.List;


public class Data {

    public String access_token;
    public String app_access_token;
    public ProfilePojo vendor_details;
    public List<FavLocationData> fav_locations = new ArrayList<>();
    public List<FavLocationData> favLocations = new ArrayList<>();
    public String ref_image;
    public String id;
    public TaskDetails task;
    public String name;
    public String code;
    public double price;
    public double cost;
    public double base_rate;
    public double max_volume;
    public double distance_rate;
    public double minimum_rate;
    public boolean isPressed = false;
    public String fleet_id;
    public String fleet_name;
    public String fleet_phone;
    public String fleet_image;
    public String fleet_thumb_image;
    public String fleet_latitude;
    public String job_address;
    public String job_latitude;
    public String job_longitude;
    public String latitude;
    public String longitude;
    public String job_pickup_address;
    public String job_pickup_latitude;
    public String job_pickup_longitude;
    public String fleet_longitude;
    public String job_hash;
    // public List<Object> app_version = null;
    // public List<FormSetting> formSettings = null;
}
