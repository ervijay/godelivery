package com.godelivery.customer.webservices.pojo;


import java.util.ArrayList;
import java.util.List;

public class PickupDropOffModel {
    public String name;
    public String email;
    public String phoneNumber;
    public String countryCode;
    public String pickUpTime;
    public String address;
    public String lat;
    public String lng;
    public String location;
    public List<String> referenceImagePath = new ArrayList<>();
}
