package com.godelivery.customer.webservices.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vijay on 23-09-2017.
 */

public class BookingModel {
    public BookingPickupDropoff delivery;
    public BookingPickupDropoff pickup;
    public String access_token;
    public String order_id;
    public String tags = "";
    //public int vehicle;
    public List<SizeModel> items = new ArrayList<>();
}
