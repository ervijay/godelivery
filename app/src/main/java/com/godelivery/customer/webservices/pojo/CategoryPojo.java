package com.godelivery.customer.webservices.pojo;


import java.util.ArrayList;
import java.util.List;

public class CategoryPojo {
    public String category_id = "0";
    public String category_name = "";
    public String category_description = "";
    public String category_image = "";
    public String banner_image = "";
    public String foreground_image = "";
    public String tag_name = "";
    public List<SubCategoryPojo> subcategories = new ArrayList<>();
}
