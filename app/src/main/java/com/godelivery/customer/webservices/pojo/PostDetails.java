package com.godelivery.customer.webservices.pojo;

import java.util.ArrayList;
import java.util.List;

public class PostDetails {
    public Integer id;
    public Integer user_id;
    public Integer subcategory_id;
    public String title = "";
    public String description = "";
    public String address = "";
    public String city = "";
    public String state = "";
    public String zip = "";
    public String country = "";
    public String lat = "";
    public String lng = "";
    public String image = "";
    public String budget = "";
    public String task_status = "";
    public Integer tasker_count;
    public String created_at = "";
    public String createdby = "";
    public String creator_image = "";
    public String fullname = "";
    public String icon_image = "";
    public String location_image = "";
    public String location = "";
    public String updated_at;
    public String deleted_at;
    public Integer post_id;
    public String is_status;
    public String is_deleted;
    public String is_contractor_bid = "false";
    public String is_consumer_acceptoffer = "false";
    public Integer bids_count = 0;
    public int category_bid_points = 0;
    public int materials_provided = 0;
    public Float distance = 0f;
    public List<Bid> bids = new ArrayList<>();
    public List<PhotoVideoPojo> media = new ArrayList<>();


}
