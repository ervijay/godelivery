package com.godelivery.customer.webservices.pojo;


public class NotificationData {
    public Integer id;
    public Integer sender_id;
    public String sender_name;
    public String sender_image;
    public Integer reciever_id;
    public Integer post_id;
    public String notification_message = "";
    public String notification_type;
    public Integer is_read = 0;
    public String status;
    public Integer is_deleted;
    public String created_at = "";
    public String updated_at;
}
