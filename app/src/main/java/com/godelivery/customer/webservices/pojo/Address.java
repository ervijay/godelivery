package com.godelivery.customer.webservices.pojo;


import java.util.ArrayList;
import java.util.List;

public class Address {
    public String formatted_address = "";
    public String name = "";
    public String place_id = "";
    public String vicinity = "";
    public Integer id;
    public String lat = "";
    public String lng = "";
    public String place_name = "";
    public Integer is_deleted;
    public String created_at;
    public String deleted_at;
    public List<AddressCompenents> address_components = new ArrayList<>();
}
