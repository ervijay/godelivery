package com.godelivery.customer.webservices.pojo;


public class ProfilePojo {

    public Integer vendor_id;
    public String first_name;
    public String last_name;
    public String email;
    public String phone_no;
    public String description;
    public Integer user_id;
    public String access_token;
    public String password;
    public String company;
    public String address;
    public Object password_reset_token;
    public Integer is_blocked;
    public String latitude;
    public String longitude;
    public String vendor_image;
    public Integer device_type;
    public String device_token;
    public String app_versioncode;
    public String app_access_token;
    public Object language;
    public String last_login_date_time;
    public String creation_datetime;
    public Object vendor_api_key;


}
