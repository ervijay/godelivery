package com.godelivery.customer.webservices.pojo;


public class Message {
    public Integer sender_id;
    public String sendername;
    public String recievername = "";
    public String sender_account_type;
    public String senderimage;
    public Integer reciever_id;
    public String recieverimage = "";
    public String reciever_account_type;
    public String message;
    public String thumbnail_url = "";
    public String message_type = "0";
    public String created_at;
    public Integer post_id;
}
