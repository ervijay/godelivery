package com.godelivery.customer.webservices.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vijay on 17-10-2017.
 */

public class MapDirectionsPojo {
    public String status;
    public List<MapGeoPoints> routes = new ArrayList<>();

}
