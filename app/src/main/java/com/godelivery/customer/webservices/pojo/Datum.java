package com.godelivery.customer.webservices.pojo;

public class Datum {
    public String customer_username;
    public String customer_phone;
    public String customer_email;
    public String job_address;
    public String job_latitude;
    public String job_longitude;
//    {"message":"Successful","status":200,"data":[{"customer_username":"Persio Lama",
//            "customer_phone":"+1 809-330-6042","customer_email":"hapelaz@gmail.com",
//            "job_address":"Páginas Amarillas, Calle Paseo de los Locutores 41, Santo Domingo, República Dominicana",
//            "job_latitude":"18.4648271","job_longitude":"-69.938649"}]}
}