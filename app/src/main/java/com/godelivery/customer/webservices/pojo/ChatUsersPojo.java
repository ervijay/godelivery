package com.godelivery.customer.webservices.pojo;


public class ChatUsersPojo {
    public Integer post_id;
    public String title = "";
    public String description = "";
    public String icon_image = "";
    public Integer subcategory_id;
    public Integer consumer_id;
    public String consumer_image = "";
    public String consumer_name = "";
    public String consumer_account_type = "";
    public String contractor_name = "";
    public Integer contractor_id;
    public String contractor_image = "";
    public String contractor_account_type = "";
    public String is_status = "0";
    public String message = "";
    public String thumbnail = "";
    public String message_type = "0";
    public String type = "";
    public Integer message_count = 0;
}
