package com.godelivery.customer.webservices;


public class WebConstants {

    //base image url
    public static final String BASE_IMAGE_URL = "http://35.164.59.243/";


    //common for all
    public static final String PARAM_ACCESS_TOKEN = "access_token";

    //for sign up api,login api,post task api,edit profile
    public static final String PARAM_NAME = "fullname";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_PHONE = "phone";
    public static final String PARAM_REG_ID = "reg_id";
    public static final String PARAM_USER_ROLE = "user_role";
    public static final String PARAM_ACCOUNT_TYPE = "account_type";
    public static final String PARAM_LATITUDE = "lat";
    public static final String PARAM_LONGITUDE = "lng";
    public static final String PARAM_FB_ID = "facebook_id";
    public static final String PARAM_GOOGLE_ID = "gplus_id";
    public static final String PARAM_IMAGE = "image";

    //post task api,edit task api
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_DESC = "description";
    public static final String PARAM_BUDGET = "budget";
    public static final String PARAM_SUBCAT_ID = "subcategory_id";
    public static final String PARAM_PERSON_COUNT = "tasker_count";
    public static final String PARAM_TASK_STATUS = "task_status";
    public static final String PARAM_CITY = "city";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_STATE = "state";
    public static final String PARAM_ZIP = "zip";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_INSURANCE = "insurance";
    public static final String PARAM_LICENCE = "company_licence";
    public static final String PARAM_COMPANY_NAME = "company_name";
    public static final String PARAM_SKILL_SET = "skill_id";
    public static final String PARAM_LOCATION = "location";
    public static final String PARAM_BUSINESS_YEARS = "business_years";
    public static final String PARAM_ABOUT_US = "about_profile";


    //post details api,place bid,edit task api,post task
    public static final String PARAM_POST_ID = "post_id";
    public static final String PARAM_BID_TEMPLATE_ID = "bid_template_id";


    //place bid api
    public static final String PARAM_BID_AMOUNT = "bid_amount";
    public static final String PARAM_BID_TEXT = "bid_text";

    //edit profile
    public static final String PARAM_OLD_PASSWORD = "old_password";
    public static final String PARAM_NEW_PASSWORD = "new_password";

    //accept offer api
    public static final String PARAM_CONTRACTOR_ID_1 = "contractor_id1";
    public static final String PARAM_CONTRACTOR_ID_2 = "contractor_id2";

    //edit task
    public static final String PARAM_STATUS = "is_status";
    public static final String PARAM_IS_DELETE = "is_deleted";

    //nearby task api
    public static final String PARAM_LAT = "latitude";
    public static final String PARAM_LNG = "longitude";

    //post media
    public static final String MEDIA_CONTENT = "media_content";
    public static final String MEDIA_TYPE = "media_type";
    public static final String THUMBNAIL = "thumbnail_url";

    //send msg api
    public static final String PARAM_SENDER_ID = "sender_id";
    public static final String PARAM_RECEIVER_ID = "reciever_id";
    public static final String PARAM_MSG = "message";
    public static final String PARAM_MSG_TYPE = "message_type";

    //update contractor
    public static final String PARAM_CONTRACTOR_ID_REJECTED = "rejected_contractor_id";

    //filter task api
    public static final String PARAM_SORT_BY = "sort";
    public static final String PARAM_DISTANCE = "distance";

    //user rating
    public static final String PARAM_ID_TO_RATE = "rate_to_id";
    public static final String PARAM_RATING = "rating";
    public static final String PARAM_REVIEW = "reviews";

    //read notification
    public static final String PARAM_IS_READ = "is_read";
    public static final String PARAM_NOTIFICATION_ID = "notification_id";

    //app-purchase
    public static final String PARAM_IN_APP_PURCHASE = "in_app_purchase";
    public static final String PARAM_USER_POINTS = "user_points";

    //chat status api
    public static final String PARAM_IS_CHAT_OPEN = "is_chat_open";

    //
    public static final String PARAM_USER_ID = "user_id";


}
