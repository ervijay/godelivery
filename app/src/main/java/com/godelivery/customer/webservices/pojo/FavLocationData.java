package com.godelivery.customer.webservices.pojo;


public class FavLocationData {
    public Integer fav_id;
    public String address;
    public String latitude;
    public String longitude;
    public Integer vendor_id;
    public Integer default_location;

}

