package com.godelivery.customer.webservices.pojo;

/**
 * Created by vijay on 07-10-2017.
 */
public class TaskDetails {
    public int job_id;
    public int pickup_job_id;
    public int delivery_job_id;
    public String job_hash;
    public String customer_address;
    public String job_pickup_name;
    public String job_pickup_address;
    public String job_token;
    public String pickup_tracking_link;
    public String delivery_tracing_link;
}
