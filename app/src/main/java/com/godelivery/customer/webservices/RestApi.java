package com.godelivery.customer.webservices;


import com.godelivery.customer.webservices.pojo.BookingModel;
import com.godelivery.customer.webservices.pojo.CommonListPojo;
import com.godelivery.customer.webservices.pojo.CommonPojo;
import com.godelivery.customer.webservices.pojo.Dropoff;
import com.godelivery.customer.webservices.pojo.MapDirectionsPojo;
import com.godelivery.customer.webservices.pojo.NameData;
import com.godelivery.customer.webservices.pojo.VehiclePojo;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface RestApi {
    String SIGN_IN = "http://34.224.237.223:8888/vendor_login";
    String SIGN_UP = "http://34.224.237.223:4000/api/v3/user/signup";
    String FORGOT_PASSWORD = "http://34.224.237.223:8888/vendor_forgot_password";
    String UPLOAD_IMAGE = "http://34.224.237.223:8888/upload_reference_images";
    String SAVE_LOCATION = "http://34.224.237.223:8888/add_fav_location";
    String GET_LOCATION = "http://34.224.237.223:8888/get_fav_location";
    String GET_VEHICLES = "http://34.224.237.223:4000/api/v3/vehicles";
    String GET_VEHICLE_COST = "http://34.224.237.223:4000/api/v3/tasks/quote";
    String ADD_BOOKING = "http://34.224.237.223:4000/api/v3/tasks";
    String CANCEL_TASK = "http://34.224.237.223:8888/cancel_vendor_task";
    String VIEW_VENDOR = "http://34.224.237.223:8888/view_job_details";
    String DRIVER_LAT_LNG = "http://34.224.237.223:8888/task_information";
    String CUSTOMER_RATING = "http://34.224.237.223:8888/customer_rating";
    String NEARBY_DRIVER = "http://52.37.58.118:8000/nearByDriver";
    String UPDATE_PROFILE = "http://34.224.237.223:4000/api/v3/user/update";
    String HISTORY = "http://34.224.237.223:8888/get_vendor_task_details";
    String LOGOUT = "http://34.224.237.223:8888/get_form_settings_via_domain";
    String CHANGE_PASSWORD = "http://34.224.237.223:8888/change_vendor_password";
    String VENDOR_SEARCH = "http://34.224.237.223:8888/get_vendor_search";

    /*String SIGN_IN="http://192.168.1.7:8001/vendor_login";
    String SIGN_UP="http://34.224.237.223:4000/api/v3/user/signup";
    String FORGOT_PASSWORD="http://192.168.1.7:8001/vendor_forgot_password";
    String UPLOAD_IMAGE="http://192.168.1.7:8001/upload_reference_images";
    String SAVE_LOCATION="http://192.168.1.7:8001/add_fav_location";
    String GET_LOCATION="http://192.168.1.7:8001/get_fav_location";
    String GET_VEHICLES="http://34.224.237.223:4000/api/v3/vehicles";
    String GET_VEHICLE_COST="http://34.224.237.223:4000/api/v3/tasks/quote";
    String ADD_BOOKING="http://34.224.237.223:4000/api/v3/tasks";
    String CANCEL_TASK="http://192.168.1.7:8001/cancel_vendor_task";
    String VIEW_VENDOR="http://192.168.1.7:8001/view_job_details";
    String DRIVER_LAT_LNG="http://192.168.1.7:8001/task_information";
    String CUSTOMER_RATING="http://192.168.1.7:8001/customer_rating";
    String NEARBY_DRIVER="http://52.37.58.118:8000/nearByDriver";
    String UPDATE_PROFILE="http://34.224.237.223:4000/api/v3/user/update";
    String HISTORY="http://192.168.1.7:8001/get_vendor_task_details";
    String LOGOUT="http://192.168.1.7:8001/get_form_settings_via_domain";
    */
    @FormUrlEncoded
    @POST(SIGN_IN)
    Call<CommonPojo> login(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST(SIGN_UP)
    Call<CommonPojo> signUp(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST(UPDATE_PROFILE)
    Call<CommonPojo> updateProfile(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    Call<CommonPojo> forgotPassword(@Field(WebConstants.PARAM_EMAIL) String email);

    @Multipart
    @POST(UPLOAD_IMAGE)
    Call<CommonPojo> uploadImage(@PartMap Map<String, RequestBody> map);


    @FormUrlEncoded
    @POST(SAVE_LOCATION)
    Call<CommonPojo> addLocation(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST(CANCEL_TASK)
    Call<CommonPojo> cancelTask(@FieldMap Map<String, String> map);


    @FormUrlEncoded
    @POST(CHANGE_PASSWORD)
    Call<CommonPojo> changePassword(@FieldMap Map<String, String> map);


    @FormUrlEncoded
    @POST(GET_LOCATION)
    Call<CommonPojo> getLocations(@Field("access_token") String token);

    @FormUrlEncoded
    @POST(LOGOUT)
    Call<CommonPojo> logout(@Field("domain_name") String domain);

    @GET(GET_VEHICLES)
    Call<VehiclePojo> getVehicles();

    @Headers("Content-Type: application/json")
    @POST(GET_VEHICLE_COST)
    Call<CommonPojo> getCost(@Body Dropoff jsonObject2);

    @Headers("Content-Type: application/json")
    @POST(ADD_BOOKING)
    Call<CommonListPojo> addBooking(@Body BookingModel bookingModel);


    @GET(HISTORY)
    Call<CommonListPojo> history(@QueryMap Map<String, String> map);

    @GET(HISTORY)
    Call<CommonListPojo> taskDetails(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST(VIEW_VENDOR)
    Call<CommonListPojo> searchDriver(@Field("job_id") String jobId);

    @FormUrlEncoded
    @POST(DRIVER_LAT_LNG)
    Call<CommonListPojo> getDriverTrack(@Field("hash") String jobHash);


    @FormUrlEncoded
    @POST(CUSTOMER_RATING)
    Call<CommonPojo> rateDriver(@Field("job_id") String jobId,
                                @Field("rating") String rating,
                                @Field("customer_comment") String comment);


    @GET("http://maps.googleapis.com/maps/api/geocode/json")
    Call<CommonPojo> getAddress(@Query("latlng") String latlng);

    @GET("https://maps.googleapis.com/maps/api/directions/json")
    Call<MapDirectionsPojo> getPath(@Query("origin") String origin,
                                    @Query("destination") String dest,
                                    @Query("key") String key);

    @FormUrlEncoded
    @POST(NEARBY_DRIVER)
    Call<CommonListPojo> nearbyDriver(@Field("latitude") String lat,
                                      @Field("longitude") String lng);

    @FormUrlEncoded
    @POST(VENDOR_SEARCH)
    Call<NameData> vendorSearch(@FieldMap Map<String, String> map);
}
