package com.godelivery.customer.webservices.pojo;


public class BidTemplate {
    public Integer id;
    public Integer user_id;
    public String template_text = "";
    public String template_title = "";
    public String created_at;
    public String updated_at;
    public String deleted_at;
    public String is_deleted;
    public boolean isSelected = false;
}
