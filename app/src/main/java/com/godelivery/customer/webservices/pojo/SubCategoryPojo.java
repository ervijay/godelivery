package com.godelivery.customer.webservices.pojo;


public class SubCategoryPojo {
    public String subcategory_id = "";
    public String subcategory_name = "";
    public String subcategory_description = "";
    public String subcategory_image = "";
    public String created_date = "";
}
