package com.godelivery.customer.webservices.pojo;


import java.util.ArrayList;
import java.util.List;

public class Bid {
    public Integer user_id;
    public String fullname = "";
    public String image = "";
    public String bid_text = "";
    public String rating = "0";
    public Integer bid_amount;
    public String joining_date = "";
    public List<Skill> skills = new ArrayList<>();
    public String address = "";
    public String company_name = "";
    public String city = "";
    public String state = "";
    public String country = "";
    public String zip = "";
    public String insurance = "";
    public String company_licence = "";
    public int user_points = 0;
    public int in_app_purchase = 0;

}
