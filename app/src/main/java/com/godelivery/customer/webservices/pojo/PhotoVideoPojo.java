package com.godelivery.customer.webservices.pojo;


import java.io.File;

public class PhotoVideoPojo {
    public String fileType = "";
    public File file = null;
    public File thumbnail = null;
    public Integer id;
    public Integer post_id;
    public String media_url = "";
    public String media_type = "";
    public String created_at;
    public String updated_at;
    public String thumbnail_url = "";
}
