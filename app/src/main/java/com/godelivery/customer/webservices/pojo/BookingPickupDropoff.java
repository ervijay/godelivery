package com.godelivery.customer.webservices.pojo;

/**
 * Created by vijay on 23-09-2017.
 */

public class BookingPickupDropoff {
    public String latitude;
    public String longitude;
    public String datetime;
    public String address;
    public BookingUserModel customer;
}
