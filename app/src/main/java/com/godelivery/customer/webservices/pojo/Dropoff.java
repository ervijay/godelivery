package com.godelivery.customer.webservices.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vijay on 23-09-2017.
 */

public class Dropoff {
    public DropOrSignupData delivery;
    public DropOrSignupData pickup;
    public String access_token;
    public int vehicle;
    public List<SizeModel> items = new ArrayList<>();
}
