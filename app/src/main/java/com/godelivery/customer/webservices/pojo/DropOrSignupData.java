package com.godelivery.customer.webservices.pojo;

/**
 * Created by vijay on 23-09-2017.
 */

public class DropOrSignupData {
    public String latitude;
    public String longitude;

    public DropOrSignupData(String lat, String lng) {
        latitude = lat;
        longitude = lng;
    }
}
