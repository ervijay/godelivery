package com.godelivery.customer.webservices.pojo;

/**
 * Created by vijay on 17-10-2017.
 */
public class MapStepsData {
    public MapsItemData polyline;
    public MapsItemData duration;
    public MapsItemData distance;
}
