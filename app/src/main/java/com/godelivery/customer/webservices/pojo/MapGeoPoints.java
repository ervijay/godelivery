package com.godelivery.customer.webservices.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vijay on 17-10-2017.
 */
public class MapGeoPoints {
    public List<MapGeoPoints> legs = new ArrayList<>();
    public List<MapStepsData> steps = new ArrayList<>();
    public MapsItemData duration;
    public MapsItemData distance;

}
