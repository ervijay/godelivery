package com.godelivery.customer.webservices;


import com.godelivery.customer.utils.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private static RestApi REST_CLIENT;

    static {
        setUpRestClient();
    }

    public static RestApi get() {
        return REST_CLIENT;
    }

    private static void setUpRestClient() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(Config.getBaseURL()).
                addConverterFactory(GsonConverterFactory.create())
                .client(getOkHttpClient()).build();
        REST_CLIENT = retrofit.create(RestApi.class);
    }

    private static OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okClient = new OkHttpClient.Builder();
        okClient.connectTimeout(5, TimeUnit.MINUTES);
        okClient.writeTimeout(5, TimeUnit.MINUTES);
        okClient.readTimeout(5, TimeUnit.MINUTES);
        return okClient.build();
    }
}
